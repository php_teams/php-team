<?php
include ('../function/include.php');
include ('../function/getInfoUser.php');
include ('../function/getcategories.php');
include ('../function/changeUserInfo.php');
if (!isset($_SESSION['loginSession'])) {
    header('location:../display/index.php');
}
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/css/account.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="../assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
        <script type="text/javascript" src="../assets/js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
        <script type="text/javascript" src="../assets/js/locales/bootstrap-datetimepicker.uk.js" charset="UTF-8"></script>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-3"><a href="../display/index.php"><h1 style="text-align: center" class="text-danger">Trang chủ</h1></a></div>
                
                <div class="col-md-9"><h1 style="text-align: center" class="text-warning">Xin chào <?php echo $sFullname ?></h1></div>
                
                <div class="col-md-3">
                    <div class="tab">
                        <button class="tablinks" onclick="openCity(event, 'UserInfo')" id="defaultOpen">Thông tin cá nhân</button>
                        <button class="tablinks" onclick="openCity(event, 'UserSecurity')">Thông tin bảo mật</button>
                        
                    </div>


                </div>
                <div class="col-md-9">
                    <div id="UserInfo" class="tabcontent">
                        <div class="userinfo active" id="userinfo">
                            <h2>Thông tin người dùng</h2>
                            <br>
                            <hr>
                            <form class="form-horizontal" action="" method="post">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Họ và tên:</label>
                                    <div class="col-sm-8">
                                        <?php echo $sFullname ?>
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="button" onclick="edit()" id="editInfo" name="" value="Chỉnh sửa">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Giới tính</label>
                                    <div class="col-sm-9">
                                        <?php echo $sGender == 1 ? "Nữ" : "Nam"; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Địa chỉ</label>
                                    <div class="col-sm-9">
                                        <?php echo $sAddress; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Ngày sinh</label>
                                    <div class="col-sm-9">
                                        <?php echo $sBirthday; ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2">Số điện thoại</label>
                                    <div class="col-sm-9">
                                        <?php echo $sPhone; ?>
                                    </div>
                                </div>

                            </form>
                            <p class="text-align-center text-danger"><?php echo isset($message) ? $message : ""; ?></p>
                        </div>
                        <div class="userinfo" id="userEdit">
                            <h2>Thay đổi thông tin cá nhân</h2>
                            <form class="form-horizontal" action="" method="post">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">Họ và tên:</label>
                                    <div class="col-sm-10">
                                        <input style="background-color: white" type="name" class="form-control" id="name" placeholder="Nhập tên của bạn" name="cName" value="<?php echo $sFullname ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">Giới tính:</label>
                                    <div class="col-sm-10">
                                        <select name="cGender">
                                            <option value="male" <?php echo $sGender == 1 ? "" : 'selected=""'; ?>>Nam</option>
                                            <option value="female" <?php echo $sGender == 0 ? "" : 'selected=""'; ?>>Nữ</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="cAddress">Địa chỉ:</label>
                                    <div class="col-sm-10">
                                        <input style="background-color: white"  type="name" class="form-control" id="phone" placeholder="Nhập địa chỉ của bạn" name="cAddress" value="<?php echo $sAddress ?>">
                                    </div>
                                </div>
                                <div class="form-group list-inline">
                                    <label class="control-label col-sm-2" for="birthday">Ngày sinh:</label>
                                    <div class="col-sm-10">


                                        <div class="controls input-append date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                            <input size="16" type="text" value="<?php echo $sBirthday ?>" readonly name="cBirthday">
                                            <span class="add-on"><i class="icon-remove"></i></span>
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                        <input type="hidden" id="dtp_input2" value="" /><br/>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-2" value="<?php echo $sPhone ?>" for="email">Số điện thoại: </label>
                                    <div class="col-sm-10">
                                        <input style="background-color: white" type="name" class="form-control" id="phone" placeholder="Nhập số điện thoại" name="cPhoneNumber" value="<?php echo $sPhone ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-9 col-sm-3">
                                        <input type="button" onclick="showInfo()" class="btn btn-default btn-success" name="" value="Hủy bỏ">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="submit" class="btn btn-default btn-success" name="submitChangeInfo" value="Xác nhận">
                                    </div>

                                </div>
                            </form>

                        </div>
                    </div>




                    <div id="UserSecurity" class="tabcontent">
                        <div class="userinfo active" id="userinfo">
                            <h2>Thông tin bảo mật</h2>
                            <br>
                            <hr>
                            <form class="form-horizontal" action="" method="post">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Tài khoản:</label>
                                    <div class="col-sm-8">
                                        <?php echo $sUsername ?>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Mật khẩu</label>
                                    <div class="col-sm-8">
                                        ********
                                    </div>
                                    <div class="col-sm-1">
                                        <input type="button" onclick="edita()" id="editInfo" name="" value="Chỉnh sửa">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Email</label>
                                    <div class="col-sm-8">
                                        <?php echo $sEmail; ?>
                                    </div>
                                    
                                </div>
                                

                            </form>
                            <p class="text-align-center text-danger"><?php echo isset($message) ? $message : ""; ?></p>
                        </div>
                        <div class="userinfo" id="changePass">
                            <h2>Đổi mật khẩu</h2>
                            <form class="form-horizontal" action="" method="post">
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">Tài khoản: </label>
                                    <div class="col-sm-10">
                                        <input style="background-color: white" type="name" class="form-control" id="name" placeholder="" name="username" value="<?php echo $_SESSION['loginSession'] ?>" readonly="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">Mật khẩu cũ:</label>
                                    <div class="col-sm-10">
                                          <input style="background-color: white" type="password" class="form-control" id="currentPassword" placeholder="Nhập mật khẩu hiện tại của bạn" name="currentPassword" value="">  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">Mật khẩu mới:</label>
                                    <div class="col-sm-10">
                                          <input style="background-color: white" type="password" class="form-control" id="newPassword" placeholder="Nhập mật khẩu hiện tại của bạn" name="newPassword" value="">  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="email">Xác nhận mật khẩu mới:</label>
                                    <div class="col-sm-10">
                                          <input style="background-color: white" type="password" class="form-control" id="sNewPassword" placeholder="Nhập mật khẩu hiện tại của bạn" name="sNewPassword" value="">  
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-9 col-sm-3">
                                        <input type="button" onclick="showInfoa()" class="btn btn-default btn-success" name="" value="Hủy bỏ">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="submit" class="btn btn-default btn-success" name="submitChangePass" value="Xác nhận">
                                    </div>

                                </div>
                            </form>

                        </div>
                        
                    </div>


                </div>
            </div>
        </div>
        <script src="../assets/js/account.js">

        </script>

    </body>
</html> 
