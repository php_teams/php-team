<?php
$title_page = $_GET['categoryName'];
include '../general/header.php';
if (isset($_GET['productId'])) {
    $sql = "SELECT * FROM category WHERE parentid IN (" . $asd . ")";
    $mainCategory = $product_categories = array();
    $mainCategory = getListCategories($conn, $sql);
    if (count($mainCategory) > 0) {

        foreach ($mainCategory as $key => $values) {
            $sql = "SELECT * FROM category WHERE parentid = $values[0]";
            $subCategory = getListCategories($conn, $sql);
            if (count($subCategory) > 0) {
                foreach ($subCategory as $key => $value) {
                    $product_categories[] = $value[0];
                }
            } else {
                $product_categories[] = $values[0];
            }
        }
    } else {
        $product_categories[] = 1;
    }
}
?>
<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
            <ul class="list-inline list-unstyled">
                <li><a href="../display/index.php">Trang chủ</a></li>
                <?php

                function getCategoryName($cateId, $conn) {
                    $sql = "SELECT * FROM category WHERE id = " . $cateId;
                    $category = $product_categories = array();
                    $category = getListCategories($conn, $sql);
                    return $category[0][1];
                }

                if (isset($_GET['categoryId'])) {
                    $listCate = array();
                    $listCate[] = $categoryId = $_GET['categoryId'];

                    $categoryName = getCategoryName($_GET['categoryId'], $conn);
                    if (isset($_GET['subCategoryId'])) {
                        $listCate[] = $subCategoryId = $_GET['subCategoryId'];
                        $subCategoryName = getCategoryName($_GET['subCategoryId'], $conn);
                        if (isset($_GET['miniSubCategoryId'])) {
                            $listCate[] = $miniSubCategoryId = $_GET['miniSubCategoryId'];
                            $miniSubCategoryName = getCategoryName($_GET['miniSubCategoryId'], $conn);
                        }
                    }
                }

                if (isset($categoryName)) {
                    if (isset($subCategoryName)) {
                        ?>

                        <li><?php echo $categoryName ?></li>
                        <?php
                        if (isset($miniSubCategoryName)) {
                            ?>
                            <li><?php echo $subCategoryId ?></li>
                            <li class='active'><?php echo $miniSubCategoryName ?></li>
                            <?php
                        } else {
                            ?>
                            <li class='active'><?php echo $subCategoryName ?></li>
                            <?php
                        }
                    } else {
                        ?>
                        <li class='active'><?php echo $categoryName ?></li>
                        <?php
                    }
                }
                if (count($listCate) > 0) {
                    $listSubcategories = array();

                    $sql = "SELECT * FROM category WHERE parentid = " . $listCate[count($listCate) - 1];
                    $sub1 = getListCategories($conn, $sql);
                    if (count($sub1) > 0) {
                        $dem = 0;
                        foreach ($sub1 as $key => $values) {
                            $sql = "SELECT * FROM category WHERE parentid = " . $values[0];
                            $sub2 = getListCategories($conn, $sql);
                            if (count($sub2) > 0) {
                                $dem1 = 0;
                                foreach ($sub2 as $key => $value) {
                                    $listSubcategories[] = $value[0];
                                }
                            } else {
                                $listSubcategories[] = $values[0];
                            }
                            $dem ++;
                        }
                    } else {
                        $listSubcategories[] = $listCate[count($listCate) - 1];
                    }
                }
                ?>

            </ul>
        </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
    <div class='container'>
        <div class='row outer-bottom-sm'>
            <div class='col-md-3 sidebar'>
                <div class="sidebar-module-container">
                    <h3 class="section-title">shop by</h3>
                    <div class="sidebar-filter">
                        <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
                        <div class="sidebar-widget wow fadeInUp outer-bottom-xs ">
                            <div class="widget-header m-t-20">
                                <h4 class="widget-title">Category</h4>
                            </div>
                            <div class="sidebar-widget-body m-t-10">
                                <div class="accordion">
                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a href="#collapseOne" data-toggle="collapse" class="accordion-toggle collapsed">
                                                Camera
                                            </a>
                                        </div><!-- /.accordion-heading -->
                                        <div class="accordion-body collapse" id="collapseOne" style="height: 0px;">
                                            <div class="accordion-inner">
                                                <ul>
                                                    <li><a href="#">gaming</a></li>
                                                    <li><a href="#">office</a></li>
                                                    <li><a href="#">kids</a></li>
                                                    <li><a href="#">for women</a></li>
                                                </ul>
                                            </div><!-- /.accordion-inner -->
                                        </div><!-- /.accordion-body -->
                                    </div><!-- /.accordion-group -->

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a href="#collapseTwo" data-toggle="collapse" class="accordion-toggle collapsed">
                                                Desktops
                                            </a>
                                        </div><!-- /.accordion-heading -->
                                        <div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">
                                            <div class="accordion-inner">
                                                <ul>
                                                    <li><a href="#">gaming</a></li>
                                                    <li><a href="#">office</a></li>
                                                    <li><a href="#">kids</a></li>
                                                    <li><a href="#">for women</a></li>
                                                </ul>
                                            </div><!-- /.accordion-inner -->
                                        </div><!-- /.accordion-body -->
                                    </div><!-- /.accordion-group -->

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a href="#collapseThree" data-toggle="collapse" class="accordion-toggle collapsed">
                                                Pants
                                            </a>
                                        </div><!-- /.accordion-heading -->
                                        <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
                                            <div class="accordion-inner">
                                                <ul>
                                                    <li><a href="#">gaming</a></li>
                                                    <li><a href="#">office</a></li>
                                                    <li><a href="#">kids</a></li>
                                                    <li><a href="#">for women</a></li>
                                                </ul>
                                            </div><!-- /.accordion-inner -->
                                        </div><!-- /.accordion-body -->
                                    </div><!-- /.accordion-group -->

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a href="#collapseFour" data-toggle="collapse" class="accordion-toggle collapsed">
                                                Bags
                                            </a>
                                        </div><!-- /.accordion-heading -->
                                        <div class="accordion-body collapse" id="collapseFour" style="height: 0px;">
                                            <div class="accordion-inner">
                                                <ul>
                                                    <li><a href="#">gaming</a></li>
                                                    <li><a href="#">office</a></li>
                                                    <li><a href="#">kids</a></li>
                                                    <li><a href="#">for women</a></li>
                                                </ul>
                                            </div><!-- /.accordion-inner -->
                                        </div><!-- /.accordion-body -->
                                    </div><!-- /.accordion-group -->

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a href="#collapseFive" data-toggle="collapse" class="accordion-toggle collapsed">
                                                Hats
                                            </a>
                                        </div><!-- /.accordion-heading -->
                                        <div class="accordion-body collapse" id="collapseFive" style="height: 0px;">
                                            <div class="accordion-inner">
                                                <ul>
                                                    <li><a href="#">gaming</a></li>
                                                    <li><a href="#">office</a></li>
                                                    <li><a href="#">kids</a></li>
                                                    <li><a href="#">for women</a></li>
                                                </ul>
                                            </div><!-- /.accordion-inner -->
                                        </div><!-- /.accordion-body -->
                                    </div><!-- /.accordion-group -->

                                    <div class="accordion-group">
                                        <div class="accordion-heading">
                                            <a href="#collapseSix" data-toggle="collapse" class="accordion-toggle collapsed">
                                                Accessories
                                            </a>
                                        </div><!-- /.accordion-heading -->
                                        <div class="accordion-body collapse" id="collapseSix" style="height: 0px;">
                                            <div class="accordion-inner">
                                                <ul>
                                                    <li><a href="#">gaming</a></li>
                                                    <li><a href="#">office</a></li>
                                                    <li><a href="#">kids</a></li>
                                                    <li><a href="#">for women</a></li>
                                                </ul>
                                            </div><!-- /.accordion-inner -->
                                        </div><!-- /.accordion-body -->
                                    </div><!-- /.accordion-group -->

                                </div><!-- /.accordion -->
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== SIDEBAR CATEGORY : END ============================================== -->
                        <!-- ============================================== PRICE SILDER============================================== -->
                        <div class="sidebar-widget outer-bottom-xs wow fadeInUp">
                            <div class="widget-header">
                                <h4 class="widget-title">Price Slider</h4>
                            </div>
                            <div class="sidebar-widget-body m-t-20">
                                <div class="price-range-holder">

                                    <span class="pull-left">$200.00</span>
                                    <span class="pull-right"><?php ?></span>

                                    <input type="text" id="amount" style="border:0; color:#666666; font-weight:bold;text-align:center;">

                                    <input type="text" class="price-slider" value="" >

                                </div><!-- /.price-range-holder -->
                                <a href="#" class="lnk btn btn-primary">Show Now</a>
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== PRICE SILDER : END ============================================== -->
                        <!-- ============================================== MANUFACTURES============================================== -->
                        <div class="sidebar-widget outer-bottom-xs wow fadeInUp">
                            <div class="widget-header">
                                <h4 class="widget-title">Manufactures</h4>
                            </div>
                            <div class="sidebar-widget-body m-t-10">
                                <ul class="list">
                                    <li><a href="#">Forever 18</a></li>
                                    <li><a href="#">Nike</a></li>
                                    <li><a href="#">Dolce & Gabbana</a></li>
                                    <li><a href="#">Alluare</a></li>
                                    <li><a href="#">Chanel</a></li>
                                    <li><a href="#">Other Brand</a></li>
                                </ul>
                                <!--<a href="#" class="lnk btn btn-primary">Show Now</a>-->
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== MANUFACTURES: END ============================================== -->
                        <!-- ============================================== COLOR============================================== -->
                        <div class="sidebar-widget wow fadeInUp outer-bottom-xs ">
                            <div class="widget-header">
                                <h4 class="widget-title">Colors</h4>
                            </div>
                            <div class="sidebar-widget-body m-t-10">
                                <ul class="list">
                                    <li><a href="#">Red</a></li>
                                    <li><a href="#">Blue</a></li>
                                    <li><a href="#">Yellow</a></li>
                                    <li><a href="#">Pink</a></li>
                                    <li><a href="#">Brown</a></li>
                                    <li><a href="#">Teal</a></li>
                                </ul>
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== COLOR: END ============================================== -->
                        <!-- ============================================== COMPARE============================================== -->
                        <div class="sidebar-widget wow fadeInUp">
                            <h3 class="section-title">Compare products</h3>
                            <div class="sidebar-widget-body">
                                <div class="compare-report">
                                    <p>You have no <span>item(s)</span> to compare</p>
                                </div><!-- /.compare-report -->
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== COMPARE: END ============================================== -->
                        <!-- ============================================== PRODUCT TAGS ============================================== -->
                        <div class="sidebar-widget product-tag wow fadeInUp">
                            <h3 class="section-title">Product tags</h3>
                            <div class="sidebar-widget-body outer-top-xs">
                                <div class="tag-list">					
                                    <a class="item" title="Phone" href="category.html">Phone</a>
                                    <a class="item active" title="Vest" href="category.html">Vest</a>
                                    <a class="item" title="Smartphone" href="category.html">Smartphone</a>
                                    <a class="item" title="Furniture" href="category.html">Furniture</a>
                                    <a class="item" title="T-shirt" href="category.html">T-shirt</a>
                                    <a class="item" title="Sweatpants" href="category.html">Sweatpants</a>
                                    <a class="item" title="Sneaker" href="category.html">Sneaker</a>
                                    <a class="item" title="Toys" href="category.html">Toys</a>
                                    <a class="item" title="Rose" href="category.html">Rose</a>
                                </div><!-- /.tag-list -->
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== PRODUCT TAGS : END ============================================== -->		            	<!-- ============================================== COLOR============================================== -->
                        <div class="sidebar-widget  wow fadeInUp outer-top-vs ">
                            <div id="advertisement" class="advertisement">
                                <div class="item bg-color">
                                    <div class="container-fluid">
                                        <div class="caption vertical-top text-left">
                                            <div class="big-text">
                                                Save<span class="big">50%</span>
                                            </div>


                                            <div class="excerpt">
                                                on selected items
                                            </div>
                                        </div><!-- /.caption -->
                                    </div><!-- /.container-fluid -->
                                </div><!-- /.item -->

                                <div class="item" style="background-image: url('../assets/images/advertisement/1.jpg');">

                                </div><!-- /.item -->

                                <div class="item bg-color">
                                    <div class="container-fluid">
                                        <div class="caption vertical-top text-left">
                                            <div class="big-text">
                                                Save<span class="big">50%</span>
                                            </div>


                                            <div class="excerpt fadeInDown-2">
                                                on selected items
                                            </div>
                                        </div><!-- /.caption -->
                                    </div><!-- /.container-fluid -->
                                </div><!-- /.item -->

                            </div><!-- /.owl-carousel -->
                        </div>

                        <!-- ============================================== COLOR: END ============================================== -->

                    </div><!-- /.sidebar-filter -->
                </div><!-- /.sidebar-module-container -->
            </div><!-- /.sidebar -->
            <div class='col-md-9'>
                <!-- ========================================== SECTION – HERO ========================================= -->

                <div id="category" class="category-carousel hidden-xs">
                    <div class="item">	
                        <div class="image">
                            <img src="../assets/images/banners/cat-banner-1.jpg" alt="" class="img-responsive">
                        </div>
                        <div class="container-fluid">
                            <div class="caption vertical-top text-left">
                                <div class="big-text">
                                    Sale
                                </div>

                                <div class="excerpt hidden-sm hidden-md">
                                    up to 50% off
                                </div>

                            </div><!-- /.caption -->
                        </div><!-- /.container-fluid -->
                    </div>
                </div>




                <!-- ========================================= SECTION – HERO : END ========================================= -->
                <div class="clearfix filters-container m-t-10">
                    <div class="row">
                        <div class="col col-sm-6 col-md-2">
                            <div class="filter-tabs">
                                <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                                    <li class="active">
                                        <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-list"></i>Grid</a>
                                    </li>
                                    <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th"></i>List</a></li>
                                </ul>
                            </div><!-- /.filter-tabs -->
                        </div><!-- /.col -->
                        <div class="col col-sm-12 col-md-6">
                            <div class="col col-sm-3 col-md-6 no-padding">
                                <div class="lbl-cnt">
                                    <span class="lbl">Sort by</span>
                                    <div class="fld inline">
                                        <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                                            <button data-toggle="dropdown" type="button" class="btn dropdown-toggle">
                                                Position <span class="caret"></span>
                                            </button>

                                            <ul role="menu" class="dropdown-menu">
                                                <li role="presentation"><a href="#">position</a></li>
                                                <li role="presentation"><a href="#">Price:Lowest first</a></li>
                                                <li role="presentation"><a href="#">Price:HIghest first</a></li>
                                                <li role="presentation"><a href="#">Product Name:A to Z</a></li>
                                            </ul>
                                        </div>
                                    </div><!-- /.fld -->
                                </div><!-- /.lbl-cnt -->
                            </div><!-- /.col -->
                            <div class="col col-sm-3 col-md-6 no-padding">
                                <div class="lbl-cnt">
                                    <span class="lbl">Show</span>
                                    <div class="fld inline">
                                        <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                                            <button data-toggle="dropdown" type="button" class="btn dropdown-toggle">
                                                1 <span class="caret"></span>
                                            </button>

                                            <ul role="menu" class="dropdown-menu">
                                                <?php
                                                $linkGet = "../display/showProductByCategory.php?";
                                                $getCategoryId = $getSubCategoryId = $getMiniSubCategoryId = $getPageNo = $getRows = "";
                                                $getCategoryId = isset($categoryId) ? "categoryId=" . $categoryId : "";
                                                $getSubCategoryId = isset($subCategoryId) ? "&subCategoryId=" . $subCategoryId : "";
                                                $getMiniSubCategoryId = isset($miniSubCategoryId) ? "&miniSubCategoryId=" . $miniSubCategoryId : "";
                                                $getPageNo = isset($page_curent) ? "&page=" . $page_curent : "";

                                                $rows_per_page = isset($_GET['showNumber']) ? $_GET['showNumber'] : 12;

                                                for ($index1 = 9; $index1 <= 27; $index1 += 3) {

                                                    $getRows = "&rows=" . $index1;
                                                    $linkQuery = $linkGet . $getCategoryId . $getSubCategoryId . $getMiniSubCategoryId . $getPageNo . $getRows;
                                                    ?>

                                                    <li role="presentation"><a href="<?php
                                                        ;
                                                        echo $linkQuery;
                                                        ?>"><?php echo $index1 ?></a></li>
                                                        <?php
                                                    }
                                                    ?>
                                            </ul>
                                        </div>
                                    </div><!-- /.fld -->
                                </div><!-- /.lbl-cnt -->
                            </div><!-- /.col -->
                        </div><!-- /.col -->
                        <div class="col col-sm-6 col-md-4 text-right">
                            <div class="pagination-container">
                                <ul class="list-inline list-unstyled">
                                    <?php
                                    $dem = 0;
                                    $sCategory = "";
                                    foreach ($listSubcategories as $key => $value) {

                                        if ($dem != count($listSubcategories) - 1) {
                                            $sCategory .= " " . $value . ", ";
                                        } else {
                                            $sCategory .= " " . $value;
                                        }

                                        $dem++;
                                    }
                                    $sql = "SELECT * FROM product WHERE category_id IN (" . $sCategory . ")";

                                    $result = mysqli_query($conn, $sql);
                                    $rows_no = mysqli_num_rows($result);
                                    $rows_per_page = isset($_GET['rows']) ? $_GET['rows'] : 12;
                                    $pages_no = intval(($rows_no - 1) / $rows_per_page) + 1;

                                    $page_curent = isset($_GET['page']) ? $_GET['page'] : 1;
                                    $linkGet = "../display/showProductByCategory.php?";
                                    $getCategoryId = $getSubCategoryId = $getMiniSubCategoryId = $getPageNo = "";
                                    $getCategoryId = isset($categoryId) ? "categoryId=" . $categoryId : "";
                                    $getSubCategoryId = isset($subCategoryId) ? "&subCategoryId=" . $subCategoryId : "";
                                    $getMiniSubCategoryId = isset($miniSubCategoryId) ? "&miniSubCategoryId=" . $miniSubCategoryId : "";
                                    $getPageNo = isset($page_curent) ? "&page=" . $page_curent : "";

                                    if ($page_curent > 1) {
                                        $getPageNo = isset($page_curent) ? "&page=" . ($page_curent - 1) : "";
                                        $getRows = isset($_GET['rows']) ? "&rows=" . ($_GET['rows']) : "";
                                        $linkQuery = $linkGet . $getCategoryId . $getSubCategoryId . $getMiniSubCategoryId . $getPageNo . $getRows;
                                        ?>
                                        <li class="prev" style="cursor: pointer">
                                            <a href="<?php echo $linkQuery; ?>">
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                        </li>
                                        <?php
                                    }

                                    for ($index = 1; $index <= $pages_no; $index++) {
                                        $getPageNo = isset($page_curent) ? "&page=" . $index : "";
                                        $getRows = isset($_GET['rows']) ? "&rows=" . ($_GET['rows']) : "";
                                        $linkQuery = $linkGet . $getCategoryId . $getSubCategoryId . $getMiniSubCategoryId . $getPageNo . $getRows;
                                        if ($index != $page_curent) {
                                            ?>
                                            <li><a href="<?php echo $linkQuery; ?>"><?php echo $index; ?></a></li>
                                            <?php
                                        } else {
                                            ?>

                                            <li class="active"><a href="<?php echo $linkQuery; ?>"><?php echo $index; ?></a></li>
                                            <?php
                                        }
                                    }

                                    if ($page_curent < $pages_no) {
                                        $getPageNo = isset($page_curent) ? "&page=" . ($page_curent + 1) : "";
                                        $getRows = isset($_GET['rows']) ? "&rows=" . ($_GET['rows']) : "";
                                        $linkQuery = $linkGet . $getCategoryId . $getSubCategoryId . $getMiniSubCategoryId . $getPageNo . $getRows;
                                        ?>
                                        <li class="next" style="cursor: pointer">
                                            <a href="<?php echo $linkQuery; ?>">
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul><!-- /.list-inline -->
                            </div><!-- /.pagination-container -->		</div><!-- /.col -->
                    </div><!-- /.row -->
                </div>
                <div class="search-result-container">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane active " id="grid-container">
                            <div class="category-product  inner-top-vs">
                                <div class="row">
                                    <?php
                                    include ('../function/getProductInfo.php');
                                    if (count($listSubcategories) > 0) {



                                        if (!$page_curent)
                                            $page_curent = 1;
                                        $start = ($page_curent - 1) * $rows_per_page;


                                        $sql = "SELECT * FROM product"
                                                . " WHERE category_id IN( " . $sCategory . ") "
                                                . " LIMIT $start,$rows_per_page";

                                        $resultList = getListProductInfo($conn, $sql);
                                    }
                                    ?>
                                    <?php
                                    foreach ($resultList as $key => $result) {
                                        ?>
                                        <div class="col-sm-6 col-md-4 wow fadeInUp">
                                            <div class="products" data-product-id="">

                                                <div class="product">		
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="<?php echo "../display/product-detail.php?productId=" . $result[0] . "&productName=" . $result[1]; ?>" ><img class="size-270-347" src="../assets/images/blank.gif" data-echo="<?php echo "../images/" . $result[5]; ?>" alt=""></a>
                                                        </div><!-- /.image -->			


                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="<?php echo "../display/product-detail.php?productId=" . $result[0] . "&productName=" . $result[1]; ?>"><?php echo $result[1]; ?></a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">	
                                                            <span class="price"><?php echo $result[3]." VND"; ?></span>
                                                            <span class="price-before-discount"></span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <a class="add-cart" href="javascript:void(0)" data-product-id="<?php echo $result[0]; ?>">
                                                                        <button class="btn btn-primary icon"  data-toggle="dropdown" type="button">
                                                                            <i class="fa fa-shopping-cart"></i>													
                                                                        </button>
                                                                        <button class="btn btn-primary"  type="button">CHO VÀO GIỎ HÀNG</button>

                                                                    </a>
                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="javascript:void(0)" title="Wishlist" data-product-id="<?php echo $result[0]; ?>">
                                                                        
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="#" title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                        <?php
                                    }
                                    ?>
                                </div><!-- /.row -->
                            </div><!-- /.category-product -->

                        </div><!-- /.tab-pane -->
                        <div class="tab-pane "  id="list-container">
                            <div class="category-product  inner-top-vs">
                                <?php
                                foreach ($resultList as $key => $result) {
                                    ?>
                                    <div class="category-product-inner wow fadeInUp">
                                        <div class="products">				
                                            <div class="product-list product" data-product-id="<?php $result[0]; ?>">
                                                <div class="row product-list-row">
                                                    <div class="col col-sm-4 col-lg-4">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <img class="size-270-347" data-echo="<?php echo "../images/" . $result[5]; ?>" src="assets/images/blank.gif" alt="">
                                                            </div>
                                                        </div><!-- /.product-image -->
                                                    </div><!-- /.col -->
                                                    <div class="col col-sm-8 col-lg-8">
                                                        <div class="product-info">
                                                            <h3 class="name"><a href="<?php echo "../display/product-detail.php?productId=" . $result[0] . "&productName=" . $result[1]; ?>"><?php echo $result[1]; ?></a></h3>
                                                            <div class="rating rateit-small"></div>
                                                            <div class="product-price">	
                                                                <span class="price">
                                                                    <?php echo $result[3]." VND"; ?>					</span>
                                                                <span class="price-before-discount"></span>

                                                            </div><!-- /.product-price -->
                                                            <div class="description m-t-10">Suspendisse posuere arcu diam, id accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum gravida eget, lacinia id purus. Suspendisse posuere arcu diam, id accumsan eros pharetra ac. Nulla enim risus, facilisis bibendum gravida eget.</div>
                                                            <div class="cart clearfix animate-effect">
                                                                <div class="action">
                                                                    <ul class="list-unstyled">
                                                                        <li class="add-cart-button btn-group">
                                                                            <a class="add-cart" href="javascript:void(0)" data-product-id="<?php echo $result[0]; ?>">
                                                                                <button class="btn btn-primary icon"  data-toggle="dropdown" type="button">
                                                                                    <i class="fa fa-shopping-cart"></i>													
                                                                                </button>
                                                                                <button class="btn btn-primary"  type="button">Add to cart</button>

                                                                            </a>
                                                                        </li>

                                                                        <li class="lnk wishlist">
                                                                            <a class="add-to-cart" href="detail.html" title="Wishlist">
                                                                                <i class="icon fa fa-heart"></i>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div><!-- /.action -->
                                                            </div><!-- /.cart -->

                                                        </div><!-- /.product-info -->	
                                                    </div><!-- /.col -->
                                                </div><!-- /.product-list-row -->
                                                <div class="tag new"><span>new</span></div>        </div><!-- /.product-list -->
                                        </div><!-- /.products -->
                                    </div><!-- /.category-product-inner -->
                                    <?php
                                }
                                ?>


                            </div><!-- /.category-product -->
                        </div><!-- /.tab-pane #list-container -->

                    </div><!-- /.tab-content -->
                    <div class="clearfix filters-container">

                        <div class="text-right">
                            <div class="pagination-container">
                                <ul class="list-inline list-unstyled pagination-demo">

                                    <?php
                                    if ($page_curent > 1) {
                                        $getPageNo = isset($page_curent) ? "&page=" . ($page_curent - 1) : "";
                                        $getRows = isset($_GET['rows']) ? "&rows=" . ($_GET['rows']) : "";
                                        $linkQuery = $linkGet . $getCategoryId . $getSubCategoryId . $getMiniSubCategoryId . $getPageNo . $getRows;
                                        ?>
                                        <li class="prev" style="cursor: pointer">
                                            <a href="<?php echo $linkQuery; ?>">
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                        </li>
                                        <?php
                                    }

                                    for ($index = 1; $index <= $pages_no; $index++) {
                                        $getPageNo = isset($page_curent) ? "&page=" . $index : "";
                                        $getRows = isset($_GET['rows']) ? "&rows=" . ($_GET['rows']) : "";
                                        $linkQuery = $linkGet . $getCategoryId . $getSubCategoryId . $getMiniSubCategoryId . $getPageNo . $getRows;
                                        if ($index != $page_curent) {
                                            ?>
                                            <li><a href="<?php echo $linkQuery; ?>"><?php echo $index; ?></a></li>
                                            <?php
                                        } else {
                                            ?>

                                            <li class="active"><a href="<?php echo $linkQuery; ?>"><?php echo $index; ?></a></li>
                                            <?php
                                        }
                                    }

                                    if ($page_curent < $pages_no) {
                                        $getPageNo = isset($page_curent) ? "&page=" . ($page_curent + 1) : "";
                                        $getRows = isset($_GET['rows']) ? "&rows=" . ($_GET['rows']) : "";
                                        $linkQuery = $linkGet . $getCategoryId . $getSubCategoryId . $getMiniSubCategoryId . $getPageNo . $getRows;
                                        ?>
                                        <li class="next" style="cursor: pointer">
                                            <a href="<?php echo $linkQuery; ?>">
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                </ul><!-- /.list-inline -->
                            </div><!-- /.pagination-container -->						    </div><!-- /.text-right -->

                    </div><!-- /.filters-container -->

                </div><!-- /.search-result-container -->

            </div><!-- /.col -->
        </div><!-- /.row -->

    </div><!-- /.body-content -->

    <?php include '../general/footer.php'; ?>