<?php
$title_page = $_GET['productName'];
include ('../general/header.php');
include ('../function/getProductInfo.php');
include ('../function/updateViewsAndWishList.php');

if (isset($_GET['productId'])) {
    $proId = $_GET['productId'];
    $proInfo = getProductInfo($conn, $proId);
    updateViews($conn, $proId);
    $vName = $vSummary = $vReview = $vNameErr = $vSummaryErr = $vReviewErr = "";
    $isError = false;
    if (isset($_POST['submitReview'])) {
        if (!empty($_POST['vName'])) {
            $vName = $_POST['vName'];
        } else {
            $isError = true;
            $vNameErr = "This field could not empty";
        }
        if (!empty($_POST['vSummary'])) {
            $vSummary = $_POST['vSummary'];
        } else {
            $isError = true;
            $vSummaryErr = "This field could not empty";
        }
        if (!empty($_POST['vReview'])) {
            $vReview = $_POST['vReview'];
        } else {
            $isError = true;
            $vReviewErr = "This field could not empty";
        }

        if (!$isError) {
            $sql = "INSERT INTO feedback(summary,message,time,name,productId)"
                    . " VALUES"
                    . "('" . $vSummary . "','" . $vReview . "',NOW(),'" . $vName . "'," . $proId . ")";
            $result = mysqli_query($conn, $sql);
            if ($result) {
                $message = "Bình luận thành công";
            } else {
                $message = "Bình luận thất bại";
            }
        }
    }
} else {
    ?>
    <!--    <script type="text/javascript">
            window.location.href = '../display/index.php';
        </script>-->
    <?php
}
?>
<html>
    <head></head>
    <body>

        <div class="body-content outer-top-xs">
            <div class='container'>
                <div class='row single-product outer-bottom-sm '>
                    <div class='col-md-3 sidebar'>
                        <div class="sidebar-module-container">
                            <!-- ==============================================CATEGORY============================================== -->
                            <div class="sidebar-widget outer-bottom-xs wow fadeInUp">
                                <h3 class="section-title">Category</h3>
                                <div class="sidebar-widget-body m-t-10">
                                    <div class="accordion">
                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="#collapseOne" data-toggle="collapse" class="accordion-toggle collapsed">
                                                    Camera
                                                </a>
                                            </div><!-- /.accordion-heading -->
                                            <div class="accordion-body collapse" id="collapseOne" style="height: 0px;">
                                                <div class="accordion-inner">
                                                    <ul>
                                                        <li><a href="#">gaming</a></li>
                                                        <li><a href="#">office</a></li>
                                                        <li><a href="#">kids</a></li>
                                                        <li><a href="#">for women</a></li>
                                                    </ul>
                                                </div><!-- /.accordion-inner -->
                                            </div><!-- /.accordion-body -->
                                        </div><!-- /.accordion-group -->

                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="#collapseTwo" data-toggle="collapse" class="accordion-toggle collapsed">
                                                    Desktops
                                                </a>
                                            </div><!-- /.accordion-heading -->
                                            <div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">
                                                <div class="accordion-inner">
                                                    <ul>
                                                        <li><a href="#">gaming</a></li>
                                                        <li><a href="#">office</a></li>
                                                        <li><a href="#">kids</a></li>
                                                        <li><a href="#">for women</a></li>
                                                    </ul>
                                                </div><!-- /.accordion-inner -->
                                            </div><!-- /.accordion-body -->
                                        </div><!-- /.accordion-group -->

                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="#collapseThree" data-toggle="collapse" class="accordion-toggle collapsed">
                                                    Pants
                                                </a>
                                            </div><!-- /.accordion-heading -->
                                            <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
                                                <div class="accordion-inner">
                                                    <ul>
                                                        <li><a href="#">gaming</a></li>
                                                        <li><a href="#">office</a></li>
                                                        <li><a href="#">kids</a></li>
                                                        <li><a href="#">for women</a></li>
                                                    </ul>
                                                </div><!-- /.accordion-inner -->
                                            </div><!-- /.accordion-body -->
                                        </div><!-- /.accordion-group -->

                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="#collapseFour" data-toggle="collapse" class="accordion-toggle collapsed">
                                                    Bags
                                                </a>
                                            </div><!-- /.accordion-heading -->
                                            <div class="accordion-body collapse" id="collapseFour" style="height: 0px;">
                                                <div class="accordion-inner">
                                                    <ul>
                                                        <li><a href="#">gaming</a></li>
                                                        <li><a href="#">office</a></li>
                                                        <li><a href="#">kids</a></li>
                                                        <li><a href="#">for women</a></li>
                                                    </ul>
                                                </div><!-- /.accordion-inner -->
                                            </div><!-- /.accordion-body -->
                                        </div><!-- /.accordion-group -->

                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="#collapseFive" data-toggle="collapse" class="accordion-toggle collapsed">
                                                    Hats
                                                </a>
                                            </div><!-- /.accordion-heading -->
                                            <div class="accordion-body collapse" id="collapseFive" style="height: 0px;">
                                                <div class="accordion-inner">
                                                    <ul>
                                                        <li><a href="#">gaming</a></li>
                                                        <li><a href="#">office</a></li>
                                                        <li><a href="#">kids</a></li>
                                                        <li><a href="#">for women</a></li>
                                                    </ul>
                                                </div><!-- /.accordion-inner -->
                                            </div><!-- /.accordion-body -->
                                        </div><!-- /.accordion-group -->

                                        <div class="accordion-group">
                                            <div class="accordion-heading">
                                                <a href="#collapseSix" data-toggle="collapse" class="accordion-toggle collapsed">
                                                    Accessories
                                                </a>
                                            </div><!-- /.accordion-heading -->
                                            <div class="accordion-body collapse" id="collapseSix" style="height: 0px;">
                                                <div class="accordion-inner">
                                                    <ul>
                                                        <li><a href="#">gaming</a></li>
                                                        <li><a href="#">office</a></li>
                                                        <li><a href="#">kids</a></li>
                                                        <li><a href="#">for women</a></li>
                                                    </ul>
                                                </div><!-- /.accordion-inner -->
                                            </div><!-- /.accordion-body -->
                                        </div><!-- /.accordion-group -->

                                    </div><!-- /.accordion -->
                                </div><!-- /.sidebar-widget-body -->
                            </div><!-- /.sidebar-widget -->
                            <!-- ============================================== CATEGORY : END ============================================== -->					<!-- ============================================== HOT DEALS ============================================== -->
                            <div class="sidebar-widget hot-deals wow fadeInUp">
                                <h3 class="section-title">hot deals</h3>
                                <div class="owl-carousel sidebar-carousel custom-carousel owl-theme outer-top-xs">

                                    <div class="item">
                                        <div class="products">
                                            <div class="hot-deal-wrapper">
                                                <div class="image">
                                                    <img src="../assets/images/hot-deals/1.jpg" alt="">
                                                </div>
                                                <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                                <div class="timing-wrapper">
                                                    <div class="box-wrapper">
                                                        <div class="date box">
                                                            <span class="key">120</span>
                                                            <span class="value">Days</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper">
                                                        <div class="hour box">
                                                            <span class="key">20</span>
                                                            <span class="value">HRS</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper">
                                                        <div class="minutes box">
                                                            <span class="key">36</span>
                                                            <span class="value">MINS</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper hidden-md">
                                                        <div class="seconds box">
                                                            <span class="key">60</span>
                                                            <span class="value">SEC</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.hot-deal-wrapper -->

                                            <div class="product-info text-left m-t-20">
                                                <h3 class="name"><a href="detail.html">Apple Iphone 5s 32GB Gold</a></h3>
                                                <div class="rating rateit-small"></div>

                                                <div class="product-price">	
                                                    <span class="price">
                                                        $600.00
                                                    </span>

                                                    <span class="price-before-discount">$800.00</span>					

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->

                                            <div class="cart clearfix animate-effect">
                                                <div class="action">

                                                    <div class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                            <i class="fa fa-shopping-cart"></i>													
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart</button>

                                                    </div>

                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div>	
                                    </div>		        
                                    <div class="item">
                                        <div class="products">
                                            <div class="hot-deal-wrapper">
                                                <div class="image">
                                                    <img src="../assets/images/hot-deals/2.jpg" alt="">
                                                </div>
                                                <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                                <div class="timing-wrapper">
                                                    <div class="box-wrapper">
                                                        <div class="date box">
                                                            <span class="key">120</span>
                                                            <span class="value">Days</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper">
                                                        <div class="hour box">
                                                            <span class="key">20</span>
                                                            <span class="value">HRS</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper">
                                                        <div class="minutes box">
                                                            <span class="key">36</span>
                                                            <span class="value">MINS</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper hidden-md">
                                                        <div class="seconds box">
                                                            <span class="key">60</span>
                                                            <span class="value">SEC</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.hot-deal-wrapper -->

                                            <div class="product-info text-left m-t-20">
                                                <h3 class="name"><a href="detail.html">Apple Iphone 5s 32GB Gold</a></h3>
                                                <div class="rating rateit-small"></div>

                                                <div class="product-price">	
                                                    <span class="price">
                                                        $600.00
                                                    </span>

                                                    <span class="price-before-discount">$800.00</span>					

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->

                                            <div class="cart clearfix animate-effect">
                                                <div class="action">

                                                    <div class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                            <i class="fa fa-shopping-cart"></i>													
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart</button>

                                                    </div>

                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div>	
                                    </div>		        
                                    <div class="item">
                                        <div class="products">
                                            <div class="hot-deal-wrapper">
                                                <div class="image">
                                                    <img src="../assets/images/hot-deals/3.jpg" alt="">
                                                </div>
                                                <div class="sale-offer-tag"><span>35%<br>off</span></div>
                                                <div class="timing-wrapper">
                                                    <div class="box-wrapper">
                                                        <div class="date box">
                                                            <span class="key">120</span>
                                                            <span class="value">Days</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper">
                                                        <div class="hour box">
                                                            <span class="key">20</span>
                                                            <span class="value">HRS</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper">
                                                        <div class="minutes box">
                                                            <span class="key">36</span>
                                                            <span class="value">MINS</span>
                                                        </div>
                                                    </div>

                                                    <div class="box-wrapper hidden-md">
                                                        <div class="seconds box">
                                                            <span class="key">60</span>
                                                            <span class="value">SEC</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.hot-deal-wrapper -->

                                            <div class="product-info text-left m-t-20">
                                                <h3 class="name"><a href="detail.html">Apple Iphone 5s 32GB Gold</a></h3>
                                                <div class="rating rateit-small"></div>

                                                <div class="product-price">	
                                                    <span class="price">
                                                        $600.00
                                                    </span>

                                                    <span class="price-before-discount">$800.00</span>					

                                                </div><!-- /.product-price -->

                                            </div><!-- /.product-info -->

                                            <div class="cart clearfix animate-effect">
                                                <div class="action">

                                                    <div class="add-cart-button btn-group">
                                                        <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                            <i class="fa fa-shopping-cart"></i>													
                                                        </button>
                                                        <button class="btn btn-primary" type="button">Add to cart</button>

                                                    </div>

                                                </div><!-- /.action -->
                                            </div><!-- /.cart -->
                                        </div>	
                                    </div>		        


                                </div><!-- /.sidebar-widget -->
                            </div>
                            <!-- ============================================== HOT DEALS: END ============================================== -->					<!-- ============================================== COLOR============================================== -->
                            <div class="sidebar-widget  wow fadeInUp outer-top-vs ">
                                <div id="advertisement" class="advertisement">
                                    <div class="item bg-color">
                                        <div class="container-fluid">
                                            <div class="caption vertical-top text-left">
                                                <div class="big-text">
                                                    Save<span class="big">50%</span>
                                                </div>


                                                <div class="excerpt">
                                                    on selected items
                                                </div>
                                            </div><!-- /.caption -->
                                        </div><!-- /.container-fluid -->
                                    </div><!-- /.item -->

                                    <div class="item" style="background-image: url('../assets/images/advertisement/1.jpg');">

                                    </div><!-- /.item -->

                                    <div class="item bg-color">
                                        <div class="container-fluid">
                                            <div class="caption vertical-top text-left">
                                                <div class="big-text">
                                                    Save<span class="big">50%</span>
                                                </div>


                                                <div class="excerpt fadeInDown-2">
                                                    on selected items
                                                </div>
                                            </div><!-- /.caption -->
                                        </div><!-- /.container-fluid -->
                                    </div><!-- /.item -->

                                </div><!-- /.owl-carousel -->
                            </div>

                            <!-- ============================================== COLOR: END ============================================== -->
                        </div>
                    </div><!-- /.sidebar -->
                    <div class='col-md-9'>
                        <div class="row  wow fadeInUp">
                            <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                                <div class="product-item-holder size-big single-product-gallery small-gallery">

                                    <div id="owl-single-product">
                                        <div class="single-product-gallery-item" id="slide1">
                                            <a data-lightbox="image-1" data-title="Gallery" href="<?php echo isset($proInfo) ? "../images/" . $proInfo[5] : ''; ?>">
                                                <img class="img-responsive" alt="" style="width: 370px; height: 450px;" src="../assets/images/blank.gif" data-echo="<?php echo isset($proInfo) ? "../images/" . $proInfo[5] : ''; ?>"" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide2">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/2.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/2.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide3">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/3.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/3.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide4">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/1.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/1.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide5">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/2.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/2.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide6">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/3.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/3.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide7">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/1.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/1.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide8">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/2.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/2.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                        <div class="single-product-gallery-item" id="slide9">
                                            <a data-lightbox="image-1" data-title="Gallery" href="../assets/images/single-product/3.jpg">
                                                <img class="img-responsive" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/3.jpg" />
                                            </a>
                                        </div><!-- /.single-product-gallery-item -->

                                    </div><!-- /.single-product-slider -->


                                    <div class="single-product-gallery-thumbs gallery-thumbs">

                                        <div id="owl-single-product-thumbnails">
                                            <div class="item">
                                                <a class="horizontal-thumb active" data-target="#owl-single-product" data-slide="1" href="#slide1">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm1.jpg" />
                                                </a>
                                            </div>

                                            <div class="item">
                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="2" href="#slide2">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm2.jpg"/>
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="3" href="#slide3">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm3.jpg" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="4" href="#slide4">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm1.jpg" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="5" href="#slide5">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm2.jpg" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="6" href="#slide6">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm3.jpg" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="7" href="#slide7">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm1.jpg" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="8" href="#slide8">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm2.jpg" />
                                                </a>
                                            </div>
                                            <div class="item">

                                                <a class="horizontal-thumb" data-target="#owl-single-product" data-slide="9" href="#slide9">
                                                    <img class="img-responsive" width="85" alt="" src="../assets/images/blank.gif" data-echo="../assets/images/single-product/sm3.jpg" />
                                                </a>
                                            </div>
                                        </div><!-- /#owl-single-product-thumbnails -->



                                    </div><!-- /.gallery-thumbs -->

                                </div><!-- /.single-product-gallery -->
                            </div><!-- /.gallery-holder -->        			
                            <div class='col-sm-6 col-md-7 product-info-block'>
                                <div class="product-info">
                                    <h1 class="name"><?php echo isset($proInfo) ? $proInfo[1] : ''; ?></h1>

                                    <div class="rating-reviews m-t-20">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="rating rateit-small"></div>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="reviews">
                                                    <a href="#" class="lnk">(<?php echo $proInfo[7] ?> Reviews)</a>
                                                </div>
                                            </div>
                                        </div><!-- /.row -->		
                                    </div><!-- /.rating-reviews -->

                                    <div class="stock-container info-container m-t-10">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <div class="stock-box">
                                                    <span class="label">Tình trạng :</span>
                                                </div>	
                                            </div>
                                            <div class="col-sm-9">
                                                <div class="stock-box">
                                                    <span class="value"><?php echo $proInfo[2] > 0 ? "Còn hàng" : "Liên hệ" ?></span>
                                                </div>	
                                            </div>
                                        </div><!-- /.row -->	
                                    </div><!-- /.stock-container -->

                                    <div class="description-container m-t-20">
                                        <?php echo $proInfo[4] ?>
                                    </div><!-- /.description-container -->

                                    <div class="price-container info-container m-t-20">
                                        <div class="row">


                                            <div class="col-sm-8">
                                                <div class="price-box">
                                                    <span class="price"><?php echo $proInfo[3] . " VND" ?></span>
                                                    <span class="price-strike">$900.00</span>
                                                </div>
                                            </div>

                                            <div class="col-sm-4">
                                                <div class="favorite-button m-t-10">
                                                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Wishlist" href="#">
                                                        <i class="fa fa-heart"></i>
                                                    </a>

                                                </div>
                                            </div>

                                        </div><!-- /.row -->
                                    </div><!-- /.price-container -->

                                    <div class="quantity-container info-container">
                                        <div class="row">

                                            <div class="col-sm-2">
                                                <span class="label">Số lượng :</span>
                                            </div>

                                            <div class="col-sm-2">
                                                <div class="cart-quantity">
                                                    <div class="quant-input">
                                                        <div class="arrows">
                                                            <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
                                                            <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
                                                        </div>
                                                        <input type="text" value="1">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-7">
                                                <a class="add-cart btn btn-default" href="javascript:void(0)" data-product-id="<?php echo $proInfo[0]; ?>"><i class="fa fa-shopping-cart inner-right-vs"></i> THÊM VÀO GIỎ HÀNG</a>
                                            </div>


                                        </div><!-- /.row -->
                                    </div><!-- /.quantity-container -->

                                    <div class="product-social-link m-t-20 text-right">
                                        <span class="social-label">Chia sẻ : </span>
                                        <div class="social-icons">
                                            <ul class="list-inline">
                                                <li><a class="fa fa-facebook" href="#"></a></li>
                                                <li><a class="fa fa-twitter" href="#"></a></li>
                                                <li><a class="fa fa-linkedin" href="#"></a></li>
                                                <li><a class="fa fa-rss" href="#"></a></li>
                                                <li><a class="fa fa-pinterest" href="#"></a></li>
                                            </ul><!-- /.social-icons -->
                                        </div>
                                    </div>




                                </div><!-- /.product-info -->
                            </div><!-- /.col-sm-7 -->
                        </div><!-- /.row -->


                        <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                            <div class="row">
                                <div class="col-sm-3">
                                    <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                        <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
                                        <li><a data-toggle="tab" href="#review">REVIEW</a></li>

                                    </ul><!-- /.nav-tabs #product-tabs -->
                                </div>
                                <div class="col-sm-9">

                                    <div class="tab-content">

                                        <div id="description" class="tab-pane in active">
                                            <div class="product-tab">
                                                <p class="text"><?php echo $proInfo[4] ?><p>
                                            </div>	
                                        </div><!-- /.tab-pane -->

                                        <div id="review" class="tab-pane">
                                            <div class="product-tab">

                                                <div class="product-reviews">
                                                    <h4 class="title">Customer Reviews</h4>
                                                    <?php
                                                    $sql = "SELECT * FROM feedback WHERE productId = '$proId' ORDER BY time DESC";
                                                    $result = mysqli_query($conn, $sql);
                                                    $rows = mysqli_num_rows($result);
                                                    $feedbackList = array();
                                                    if ($rows > 0 ) {
                                                        while ($row = $result->fetch_assoc()) {
                                                            $fId = $row["id"];
                                                            $fName = $row["name"];

                                                            $fSummary = $row["summary"];
                                                            $fMessage = $row["message"];
                                                            $fTime = $row["time"];
                                                            $subFeedbackList = array();
                                                            $subFeedbackList[] = $fId;
                                                            $subFeedbackList[] = $fName;

                                                            $subFeedbackList[] = $fSummary;
                                                            $subFeedbackList[] = $fMessage;
                                                            $subFeedbackList[] = $fTime;
                                                            $feedbackList[] = $subFeedbackList;
                                                        }
                                                    }
                                                    if (count($feedbackList)) {
                                                        foreach ($feedbackList as $key => $value) {
                                                            ?>
                                                            <div class="reviews">
                                                                <div class="review">
                                                                    
                                                                    <div class="review-title"><span class="summary"><?php echo $value[2]; ?></span><span class="date"><i class="fa fa-calendar"></i><span><?php echo $value[4]; ?></span></span></div>
                                                                    <div class="text">"<?php echo $value[3]; ?>"</div>
                                                                    <div class="author m-t-15"><i class="fa fa-pencil-square-o"></i> <span class="name"><?php echo $value[1]; ?></span></div>													</div>

                                                            </div><!-- /.reviews -->
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div><!-- /.product-reviews -->



                                                <div class="product-add-review">
                                                    <h4 class="title">Write your own review</h4>
                                                    <div class="review-table">
                                                        <div class="table-responsive">
                                                            <table class="table table-bordered">	
                                                                <thead>
                                                                    <tr>
                                                                        <th class="cell-label">&nbsp;</th>
                                                                        <th>1 sao</th>
                                                                        <th>2 sao</th>
                                                                        <th>3 sao</th>
                                                                        <th>4 sao</th>
                                                                        <th>5 sao</th>
                                                                    </tr>
                                                                </thead>	
                                                                <tbody>
                                                                    <tr>
                                                                <form id="vQuantity">
                                                                    <td class="cell-label">Đánh giá: </td>
                                                                    <td><input type="radio" name="quatity" class="radio" value="1"></td>
                                                                    <td><input type="radio" name="quatity" class="radio" value="2"></td>
                                                                    <td><input type="radio" name="quatity" class="radio" value="3"></td>
                                                                    <td><input type="radio" name="quatity" class="radio" value="4"></td>
                                                                    <td><input type="radio" name="quatity" class="radio" value="5" checked=""></td>
                                                                </form>
                                                                </tr>

                                                                </tbody>
                                                            </table><!-- /.table .table-bordered -->
                                                        </div><!-- /.table-responsive -->
                                                    </div><!-- /.review-table -->

                                                    <div class="review-form">
                                                        <div class="form-container">
                                                            <form role="form" class="cnt-form" action="" method="post">

                                                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputName">Tên của bạn <span class="astk">*</span></label>
                                                                            <input type="text" class="form-control txt" id="exampleInputName" placeholder="" name="vName">
                                                                        </div><!-- /.form-group -->
                                                                        <div class="form-group">
                                                                            <label for="exampleInputSummary">Chủ đề <span class="astk">*</span></label>
                                                                            <input type="text" class="form-control txt" id="exampleInputSummary" placeholder="" name="vSummary">
                                                                        </div><!-- /.form-group -->
                                                                    </div>

                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="exampleInputReview">Đánh giá <span class="astk">*</span></label>
                                                                            <textarea class="form-control txt txt-review" id="exampleInputReview" rows="4" placeholder="" name="vReview"></textarea>
                                                                        </div><!-- /.form-group -->
                                                                    </div>
                                                                </div><!-- /.row -->

                                                                <div class="action text-right">
                                                                    <input type="submit" class="btn btn-primary btn-upper" id="reviews" value="Đánh giá" name="submitReview">

                                                                </div><!-- /.action -->

                                                            </form><!-- /.cnt-form -->

                                                        </div><!-- /.form-container -->
                                                    </div><!-- /.review-form -->

                                                </div><!-- /.product-add-review -->										

                                            </div><!-- /.product-tab -->
                                        </div><!-- /.tab-pane -->
                                    </div><!-- /.tab-content -->
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- /.product-tabs -->

                        <!-- ============================================== UPSELL PRODUCTS ============================================== -->
                        <section class="section featured-product wow fadeInUp">
                            <h3 class="section-title">upsell products</h3>
                            <?php
                            $productInfo = getProductInfo($conn, $proId);
                            $sql = "SELECT * FROM product WHERE category_id = " . $productInfo[6] . " ORDER BY RAND() LIMIT 5";
                            ?>
                            <div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">
                                <?php
                                $listProduct = getListProductInfo($conn, $sql);
                                foreach ($listProduct as $key => $value) {
                                    ?>
                                    <div class="item item-carousel">
                                        <div class="products">

                                            <div class="product">		
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="../display/product-detail.php?productId=<?php echo $value[0] ?>"><img class="size-195-243" src="../assets/images/blank.gif" data-echo="<?php echo "../images/" . $value[5]; ?>" alt=""></a>
                                                    </div><!-- /.image -->			


                                                </div><!-- /.product-image -->


                                                <div class="product-info text-left">
                                                    <h3 class="name"><a href="../display/product-detail.php?productId=<?php echo $value[0] ?>"><?php echo $value[1]; ?></a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="description"></div>

                                                    <div class="product-price">	
                                                        <span class="price">
                                                            $650.99				</span>
                                                        <span class="price-before-discount"><?php echo $value[3] . " VND"; ?></span>

                                                    </div><!-- /.product-price -->

                                                </div><!-- /.product-info -->
                                                <div class="cart clearfix animate-effect">
                                                    <div class="action">
                                                        <ul class="list-unstyled">
                                                            <li class="add-cart-button btn-group">
                                                                <button class="btn btn-primary icon" data-toggle="dropdown" type="button">
                                                                    <i class="fa fa-shopping-cart"></i>													
                                                                </button>
                                                                <button class="btn btn-primary" type="button">Add to cart</button>

                                                            </li>

                                                            <li class="lnk wishlist">
                                                                <a class="add-to-cart" href="#" title="Wishlist">
                                                                    <i class="icon fa fa-heart"></i>
                                                                </a>
                                                            </li>


                                                        </ul>
                                                    </div><!-- /.action -->
                                                </div><!-- /.cart -->
                                            </div><!-- /.product -->

                                        </div><!-- /.products -->
                                    </div><!-- /.item -->
                                    <?php
                                }
                                ?>


                            </div><!-- /.home-owl-carousel -->
                        </section><!-- /.section -->
                        <!-- ============================================== UPSELL PRODUCTS : END ============================================== -->

                    </div><!-- /.col -->
                    <div class="clearfix"></div>
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.body-content -->
    </body>
    <?php include '../general/footer.php'; ?>
</html>
