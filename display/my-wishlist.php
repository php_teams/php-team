<?php
$title_page = 'Trang chủ';
include '../general/header.php';
include ('../function/getProductInfo.php');
$sql = "SELECT * FROM wishlist WHERE userId = " . $sId;
$result = mysqli_query($conn, $sql);
$rows = mysqli_num_rows($result);
$wishList = array();
if ($rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $id = $row["id"];
        $pId = $row["productId"];
        $wish = array();
        $wish[] = $id;
        $wish[] = $pId;
        $wishList[] = $wish;
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <div class="breadcrumb">
            <div class="container">
                <div class="breadcrumb-inner">
                    <ul class="list-inline list-unstyled">
                        <li><a href="home.html">Home</a></li>
                        <li class='active'>Wishlish</li>
                    </ul>
                </div><!-- /.breadcrumb-inner -->
            </div><!-- /.container -->
        </div><!-- /.breadcrumb -->

        <div class="body-content outer-top-bd">
            <div class="container">
                <div class="my-wishlist-page inner-bottom-sm">
                    <div class="row">
                        <div class="col-md-12 my-wishlist">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th colspan="4">my wishlist</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($wishList as $key => $value) {
                                            $productInformation = getProductInfo($conn, $value[0]);
                                            ?>
                                            <tr>
                                                <td class="col-md-2"><img style="width: 78px;height: 122px" src="<?php echo "../images/" . $productInformation[5]; ?>" alt="imga"></td>
                                                <td class="col-md-6">
                                                    <div class="product-name"><a href="<?php echo "../display/product-detail.php?productId=" . $productInformation[0] . "&productName=" . $productInformation[1]; ?>"><?php echo $productInformation[1]; ?></a></div>
                                                    <div class="rating">
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star rate"></i>
                                                        <i class="fa fa-star non-rate"></i>
                                                        <span class="review"><?php echo $productInformation[7]." lượt xem"; ?></span>
                                                    </div>
                                                    <div class="price">
                                                        
                                                        <span><?php echo $productInformation[3]." VND"; ?></span>
                                                    </div>
                                                </td>
                                                <td class="col-md-2">
                                                    <a class="add-cart" href="javascript:void(0)" data-product-id="<?php echo $productInformation[0]; ?>">Thêm vào giỏ hàng</a>
                                                </td>
                                                <td class="col-md-2 close-btn">
                                                    <a href="#" class=""><i class="fa fa-times"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>			</div><!-- /.row -->
                </div><!-- /.sigin-in-->
                <!-- ============================================== BRANDS CAROUSEL ============================================== -->
                <div id="brands-carousel" class="logo-slider wow fadeInUp">

                    <h3 class="section-title">Our Brands</h3>
                    <div class="logo-slider-inner">	
                        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                            <div class="item m-t-15">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item m-t-10">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->

                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                                </a>	
                            </div><!--/.item-->
                        </div><!-- /.owl-carousel #logo-slider -->
                    </div><!-- /.logo-slider-inner -->

                </div><!-- /.logo-slider -->
                <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
        </div><!-- /.body-content --> 
    </body>
    <?php include '../general/footer.php'; ?>
</html>














