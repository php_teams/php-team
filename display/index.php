<?php
$title_page = 'Trang chủ';
include '../general/header.php';
include ('../function/getProductInfo.php');
?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <!-- ============================================== HEADER : END ============================================== -->
        <div class="body-content outer-top-xs" id="top-banner-and-menu">
            <div class="container">
                <div class="row">
                    <!-- ============================================== SIDEBAR ============================================== -->	
                    <div class="col-xs-12 col-sm-12 col-md-3 sidebar">
                        <!-- ============================================== COLOR============================================== -->
                        <div class="sidebar-widget  wow fadeInUp outer-top-vs ">
                            <div id="advertisement" class="advertisement">
                                <div class="item bg-color">
                                    <div class="container-fluid">
                                        <div class="caption vertical-top text-left">
                                            <div class="big-text">
                                                Save<span class="big">50%</span>
                                            </div>


                                            <div class="excerpt">
                                                on selected items
                                            </div>
                                        </div><!-- /.caption -->
                                    </div><!-- /.container-fluid -->
                                </div><!-- /.item -->

                                <div class="item" style="background-image: url('assets/images/advertisement/1.jpg');">

                                </div><!-- /.item -->

                                <div class="item bg-color">
                                    <div class="container-fluid">
                                        <div class="caption vertical-top text-left">
                                            <div class="big-text">
                                                Save<span class="big">50%</span>
                                            </div>


                                            <div class="excerpt fadeInDown-2">
                                                on selected items
                                            </div>
                                        </div><!-- /.caption -->
                                    </div><!-- /.container-fluid -->
                                </div><!-- /.item -->

                            </div><!-- /.owl-carousel -->
                        </div>

                        <!-- ============================================== COLOR: END ============================================== -->


                        <!-- ============================================== SPECIAL OFFER ============================================== -->

                        <div class="sidebar-widget outer-bottom-small wow fadeInUp">
                            <h3 class="section-title">Đề nghị</h3>

                            <div class="sidebar-widget-body outer-top-xs">
                                <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                                    <!-- Start-->
                                    <?php
                                    $sql = "SELECT * FROM product WHERE status = 'offer'";
                                    $offerProduct = array();
                                    $offerProduct = getListProductInfo($conn, $sql);
                                    $dem = 0;
                                    for ($index = 0; $index < $countIndex = (count($offerProduct)) % 3 == 0 ? (count($offerProduct) / 3) : (intval(count($offerProduct) / 3) + 1); $index++) {
                                        ?>  
                                        <div class="item">
                                            <div class="products special-product">
                                                <?php
                                                for ($index1 = $dem; $index1 < ($dem + 3); $index1++) {
                                                    if ($index1 >= count($offerProduct)) {
                                                        break;
                                                    }
                                                    ?>
                                                    <div data-product-id="<?php echo $offerProduct[$index1][0] ?>">
                                                        <div class="product">
                                                            <div class="product-micro">
                                                                <div class="row product-micro-row">
                                                                    <div class="col col-xs-5">
                                                                        <div class="product-image">
                                                                            <div class="image">
                                                                                <a href="../display/product-detail.php?productId=<?php echo $offerProduct[$index1][0] . "&productName=" . $offerProduct[$index1][1] ?>" data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                                    <img  src="<?php echo '../images/' . $offerProduct[$index1][5]; ?>" class="size-100-120" alt="">
                                                                                    <div class="zoom-overlay"></div>
                                                                                </a>					
                                                                            </div><!-- /.image -->


                                                                        </div><!-- /.product-image -->
                                                                    </div><!-- /.col -->
                                                                    <div class="col col-xs-7">
                                                                        <div class="product-info">
                                                                            <h3 class="name"><a href="../display/product-detail.php?productId=<?php echo $offerProduct[$index1][0] . "&productName=" . $offerProduct[$index1][1] ?>"><?php echo $offerProduct[$index1][1]; ?></a></h3>
                                                                            <div class="rating rateit-small"></div>
                                                                            <div class="product-price">	
                                                                                <span class="price">
                                                                                    <?php echo $offerProduct[$index1][3] . " VND"; ?>			
                                                                                </span>

                                                                            </div><!-- /.product-price -->
                                                                            <div class="action"><a href="javascript:void(0)" class="lnk btn btn-primary add-cart" data-product-id="<?php echo $offerProduct[$index1][0]; ?>">Add To Cart</a></div>
                                                                        </div>
                                                                    </div><!-- /.col -->
                                                                </div><!-- /.product-micro-row -->
                                                            </div><!-- /.product-micro -->
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                $dem+=3;
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== SPECIAL OFFER : END ============================================== -->
                        <!-- ============================================== SPECIAL OFFER ============================================== -->

                        <div class="sidebar-widget outer-bottom-small wow fadeInUp">
                            <h3 class="section-title">Khuyến mãi</h3>

                            <div class="sidebar-widget-body outer-top-xs">
                                <div class="owl-carousel sidebar-carousel special-offer custom-carousel owl-theme outer-top-xs">
                                    <!-- Start-->
                                    <?php
                                    $sql = "SELECT * FROM product WHERE status = 'sale'";
                                    $SaleProduct = array();
                                    $SaleProduct = getListProductInfo($conn, $sql);
                                    $dem = 0;
                                    for ($index = 0; $index < $countIndex = (count($SaleProduct)) % 3 == 0 ? (count($SaleProduct) / 3) : (intval(count($SaleProduct) / 3) + 1); $index++) {
                                        ?>  
                                        <div class="item">
                                            <div class="products special-product">
                                                <?php
                                                for ($index1 = $dem; $index1 < ($dem + 3); $index1++) {
                                                    if ($index1 >= count($SaleProduct)) {
                                                        break;
                                                    }
                                                    ?>
                                                    <div data-product-id="<?php echo $SaleProduct[$index1][0] ?>">
                                                        <div class="product">
                                                            <div class="product-micro">
                                                                <div class="row product-micro-row">
                                                                    <div class="col col-xs-5">
                                                                        <div class="product-image">
                                                                            <div class="image">
                                                                                <a href="../display/product-detail.php?productId=<?php echo $SaleProduct[$index1][0] . "&productName=" . $SaleProduct[$index1][1] ?>" data-lightbox="image-1" data-title="Nunc ullamcors">
                                                                                    <img data-echo="<?php echo '../images/' . $SaleProduct[$index1][5]; ?>" src="../assets/images/blank.gif" class="size-100-120" alt="">
                                                                                    <div class="zoom-overlay"></div>
                                                                                </a>					
                                                                            </div><!-- /.image -->


                                                                        </div><!-- /.product-image -->
                                                                    </div><!-- /.col -->
                                                                    <div class="col col-xs-7">
                                                                        <div class="product-info">
                                                                            <h3 class="name"><a href="
                                                                                                ../display/product-detail.php?productId=<?php echo $SaleProduct[$index1][0] . "&productName=" . $SaleProduct[$index1][1] ?>">
                                                                                                    <?php echo $SaleProduct[$index1][1]; ?>
                                                                                </a>
                                                                            </h3>
                                                                            <div class="rating rateit-small"></div>
                                                                            <div class="product-price">	
                                                                                <span class="price">
                                                                                    <?php echo $SaleProduct[$index1][3] . " VND"; ?>			
                                                                                </span>

                                                                            </div><!-- /.product-price -->
                                                                            <div class="action"><a href="javascript:void(0)" class="lnk btn btn-primary add-cart" data-product-id="<?php echo $SaleProduct[$index1][0]; ?>">Add To Cart</a></div>
                                                                        </div>
                                                                    </div><!-- /.col -->
                                                                </div><!-- /.product-micro-row -->
                                                            </div><!-- /.product-micro -->
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                $dem+=3;
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>

                                </div>
                            </div><!-- /.sidebar-widget-body -->
                        </div><!-- /.sidebar-widget -->
                        <!-- ============================================== SPECIAL OFFER : END ============================================== -->

                    </div><!-- /.sidemenu-holder -->
                    <!-- ============================================== SIDEBAR : END ============================================== -->

                    <!-- ============================================== CONTENT ============================================== -->
                    <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
                        <!-- ========================================== SECTION – HERO ========================================= -->

                        <div id="hero">
                            <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">

                                <div class="item" style="background-image: url(../assets/images/sliders/04.png);">
                                    <div class="container-fluid">
                                        <div class="caption bg-color vertical-center text-left">
                                            <!--                                            <div class="big-text fadeInDown-1">
                                                                                            The new <span class="highlight">imac</span> for you
                                                                                        </div>
                                            
                                                                                        <div class="excerpt fadeInDown-2 hidden-xs">
                                            
                                                                                            <span>S, M Now Starting At $1099 </span>
                                                                                            <span>XL Starting At $1799</span>
                                                                                        </div>-->
                                            <div class="button-holder fadeInDown-3">
                                                <a href="index.php?page=single-product" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a>
                                            </div>
                                        </div><!-- /.caption -->
                                    </div><!-- /.container-fluid -->
                                </div><!-- /.item -->

                                <div class="item" style="background-image: url(../assets/images/sliders/005.png);">
                                    <div class="container-fluid">
                                        <div class="caption bg-color vertical-center text-left">
                                            <!--                                            <div class="big-text fadeInDown-1">
                                                                                            The new <span class="highlight">imac</span> for you
                                                                                        </div>
                                            
                                                                                        <div class="excerpt fadeInDown-2 hidden-xs">
                                            
                                                                                            <span>S,M Now Starting At $1099 </span>
                                                                                            <span>XL Starting At $1799</span>
                                                                                        </div>-->
                                            <div class="button-holder fadeInDown-3">
                                                <a href="index.php?page=single-product" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Shop Now</a>
                                            </div>
                                        </div><!-- /.caption -->
                                    </div><!-- /.container-fluid -->
                                </div><!-- /.item -->



                            </div><!-- /.owl-carousel -->
                        </div>

                        <!-- ========================================= SECTION – HERO : END ========================================= -->	

                        <!-- ============================================== INFO BOXES ============================================== -->
                        <div class="info-boxes wow fadeInUp">
                            <div class="info-boxes-inner">
                                <div class="row">
                                    <div class="col-md-6 col-sm-4 col-lg-4">
                                        <div class="info-box">
                                            <div class="row">
                                                <div class="col-xs-2">
                                                    <i class="icon fa fa-dollar"></i>
                                                </div>
                                                <div class="col-xs-10">
                                                    <h4 class="info-box-heading green">money back</h4>
                                                </div>
                                            </div>	
                                            <h6 class="text">30 Day Money Back Guarantee.</h6>
                                        </div>
                                    </div><!-- .col -->

                                    <div class="hidden-md col-sm-4 col-lg-4">
                                        <div class="info-box">
                                            <div class="row">
                                                <div class="col-xs-2">
                                                    <i class="icon fa fa-truck"></i>
                                                </div>
                                                <div class="col-xs-10">
                                                    <h4 class="info-box-heading orange">free shipping</h4>
                                                </div>
                                            </div>
                                            <h6 class="text">free ship-on oder over $600.00</h6>	
                                        </div>
                                    </div><!-- .col -->

                                    <div class="col-md-6 col-sm-4 col-lg-4">
                                        <div class="info-box">
                                            <div class="row">
                                                <div class="col-xs-2">
                                                    <i class="icon fa fa-gift"></i>
                                                </div>
                                                <div class="col-xs-10">
                                                    <h4 class="info-box-heading red">Special Sale</h4>
                                                </div>
                                            </div>
                                            <h6 class="text">All items-sale up to 20% off </h6>	
                                        </div>
                                    </div><!-- .col -->
                                </div><!-- /.row -->
                            </div><!-- /.info-boxes-inner -->

                        </div><!-- /.info-boxes -->
                        <!-- ============================================== INFO BOXES : END ============================================== -->
                        <!-- ============================================== Clothes PRODUCTS ============================================== -->
                        <section class="section wow fadeInUp">
                            <h3 class="section-title">Quần áo</h3>
                            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                                <?php
                                $sql = "SELECT * FROM category WHERE parentid = 1 ORDER BY RAND()";
                                $mainCategory = $product_categories = array();
                                $mainCategory = getListCategories($conn, $sql);
                                if (count($mainCategory) > 0) {

                                    foreach ($mainCategory as $key => $values) {
                                        $sql = "SELECT * FROM category WHERE parentid = $values[0]";
                                        $subCategory = getListCategories($conn, $sql);
                                        if (count($subCategory) > 0) {
                                            foreach ($subCategory as $key => $value) {
                                                $product_categories[] = $value[0];
                                            }
                                        } else {
                                            $product_categories[] = $values[0];
                                        }
                                    }
                                } else {
                                    $product_categories[] = 1;
                                }

                                foreach ($product_categories as $key => $values) {

                                    $sql = "SELECT * FROM product WHERE category_id = $values ORDER BY RAND() LIMIT 1";
                                    $listProduct = getListProductInfo($conn, $sql);

                                    foreach ($listProduct as $key => $value) {
                                        ?>
                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product" data-product-id="<?php echo $value[0] ?>">		
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>"><img class="size-195-243" src="<?php echo "../images/" . $value[5]; ?>" alt=""></a>
                                                        </div><!-- /.image -->			       		   
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>"><?php echo $value[1]; ?></a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">	
                                                            <span class="price">
                                                                <?php echo $value[3] . " VND"; ?>			</span>
                                                            <span class="price-before-discount"><?php echo $value[3] . " VND"; ?></span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <a class="add-cart" href="javascript:void(0)" data-product-id="<?php echo $value[0]; ?>">
                                                                        <button class="btn btn-primary icon"  data-toggle="dropdown" type="button">
                                                                            <i class="fa fa-shopping-cart"></i>													
                                                                        </button>
                                                                        <button class="btn btn-primary"  type="button">Thêm vào giỏ hàng</button>

                                                                    </a>
                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-wishlist" href="javascript:void(0)" title="Wishlist" data-product-id="<?php echo $value[0]; ?>">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                        <?php
                                    }
                                }
                                ?>


                            </div><!-- /.home-owl-carousel -->
                        </section><!-- /.section -->
                        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

                        <!-- ============================================== Clothes PRODUCTS ============================================== -->
                        <section class="section wow fadeInUp">
                            <h3 class="section-title">Thiết bị tập</h3>
                            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                                <?php
                                $sql = "SELECT * FROM category WHERE parentid = 2";
                                $mainCategory = $product_categories = array();
                                $mainCategory = getListCategories($conn, $sql);
                                if (count($mainCategory) > 0) {

                                    foreach ($mainCategory as $key => $values) {
                                        $sql = "SELECT * FROM category WHERE parentid = $values[0]";
                                        $subCategory = getListCategories($conn, $sql);
                                        if (count($subCategory) > 0) {
                                            foreach ($subCategory as $key => $value) {
                                                $product_categories[] = $value[0];
                                            }
                                        } else {
                                            $product_categories[] = $values[0];
                                        }
                                    }
                                } else {
                                    $product_categories[] = 1;
                                }

                                foreach ($product_categories as $key => $values) {

                                    $sql = "SELECT * FROM product WHERE category_id = $values ORDER BY RAND() LIMIT 1";
                                    $listProduct = getListProductInfo($conn, $sql);
                                    foreach ($listProduct as $key => $value) {
                                        ?>
                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">		
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>"><img class="size-195-243" src="<?php echo "../images/" . $value[5]; ?>" alt=""></a>
                                                        </div><!-- /.image -->			       		   
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>"><?php echo $value[1]; ?></a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">	
                                                            <span class="price">
                                                                <?php echo $value[3] . " VND"; ?>			</span>
                                                            <span class="price-before-discount"><?php echo $value[3] . " VND"; ?></span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <a class="add-cart" href="javascript:void(0)" data-product-id="<?php echo $value[0]; ?>">
                                                                        <button class="btn btn-primary icon"  data-toggle="dropdown" type="button">
                                                                            <i class="fa fa-shopping-cart"></i>													
                                                                        </button>
                                                                        <button class="btn btn-primary"  type="button">Add to cart</button>

                                                                    </a>
                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="#" title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="#" title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                        <?php
                                    }
                                }
                                ?>


                            </div><!-- /.home-owl-carousel -->
                        </section><!-- /.section -->
                        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

                        <!-- ============================================== Clothes PRODUCTS ============================================== -->
                        <section class="section wow fadeInUp">
                            <h3 class="section-title">Tennis, Golf, Bơi lội, Bóng đá</h3>
                            <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
                                <?php
                                $sql = "SELECT * FROM category WHERE parentid = 3 OR parentid = 4 OR parentid = 5 OR parentid = 6";
                                $mainCategory = $product_categories = array();
                                $mainCategory = getListCategories($conn, $sql);
                                if (count($mainCategory) > 0) {

                                    foreach ($mainCategory as $key => $values) {
                                        $sql = "SELECT * FROM category WHERE parentid = $values[0]";
                                        $subCategory = getListCategories($conn, $sql);
                                        if (count($subCategory) > 0) {
                                            foreach ($subCategory as $key => $value) {
                                                $product_categories[] = $value[0];
                                            }
                                        } else {
                                            $product_categories[] = $values[0];
                                        }
                                    }
                                } else {
                                    $product_categories[] = 1;
                                }

                                foreach ($product_categories as $key => $values) {

                                    $sql = "SELECT * FROM product WHERE category_id = $values ORDER BY RAND() LIMIT 1";
                                    $listProduct = getListProductInfo($conn, $sql);
                                    foreach ($listProduct as $key => $value) {
                                        ?>
                                        <div class="item item-carousel">
                                            <div class="products">

                                                <div class="product">		
                                                    <div class="product-image">
                                                        <div class="image">
                                                            <a href="<?php echo "../images/" . $value[5]; ?>"><img class="size-195-243" src="<?php echo "../images/" . $value[5]; ?>" alt=""></a>
                                                        </div><!-- /.image -->			       		   
                                                    </div><!-- /.product-image -->


                                                    <div class="product-info text-left">
                                                        <h3 class="name"><a href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>"><?php echo $value[1]; ?></a></h3>
                                                        <div class="rating rateit-small"></div>
                                                        <div class="description"></div>

                                                        <div class="product-price">	
                                                            <span class="price">
                                                                <?php echo $value[3] . " VND"; ?>			</span>
                                                            <span class="price-before-discount"><?php echo $value[3] . " VND"; ?></span>

                                                        </div><!-- /.product-price -->

                                                    </div><!-- /.product-info -->
                                                    <div class="cart clearfix animate-effect">
                                                        <div class="action">
                                                            <ul class="list-unstyled">
                                                                <li class="add-cart-button btn-group">
                                                                    <a class="add-cart" href="javascript:void(0)" data-product-id="<?php echo $value[0]; ?>">
                                                                        <button class="btn btn-primary icon"  data-toggle="dropdown" type="button">
                                                                            <i class="fa fa-shopping-cart"></i>													
                                                                        </button>
                                                                        <button class="btn btn-primary"  type="button">Add to cart</button>

                                                                    </a>
                                                                </li>

                                                                <li class="lnk wishlist">
                                                                    <a class="add-to-cart" href="#" title="Wishlist">
                                                                        <i class="icon fa fa-heart"></i>
                                                                    </a>
                                                                </li>

                                                                <li class="lnk">
                                                                    <a class="add-to-cart" href="#" title="Compare">
                                                                        <i class="fa fa-retweet"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div><!-- /.action -->
                                                    </div><!-- /.cart -->
                                                </div><!-- /.product -->

                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                        <?php
                                    }
                                }
                                ?>


                            </div><!-- /.home-owl-carousel -->
                        </section><!-- /.section -->
                        <!-- ============================================== FEATURED PRODUCTS : END ============================================== -->

                    </div><!-- /.homebanner-holder -->
                    <!-- ============================================== CONTENT : END ============================================== -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /#top-banner-and-menu -->
    </body>
    <?php include '../general/footer.php'; ?>
</html>


