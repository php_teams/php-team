<?php
$title_page = "Tìm kiếm " . $_GET['search'];
include '../general/header.php';
include ('../function/getProductInfo.php');
$sql = "SELECT * FROM product WHERE MATCH(name, description)
    AGAINST('" . $_GET['search'] . "' IN NATURAL LANGUAGE MODE) ORDER BY RAND()";
$listProduct = getListProductInfo($conn, $sql);
?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <!-- ============================================== HEADER : END ============================================== -->
        <div class="body-content outer-top-xs" id="top-banner-and-menu">
            <div class="container">
                <div class="row">
                    <!-- ============================================== CONTENT ============================================== -->
                    <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
                        <!-- ============================================== Clothes PRODUCTS ============================================== -->
                        <?php
                        foreach ($listProduct as $key => $value) {
                            ?>
                            <div class="col-sm-6 col-md-4 wow fadeInUp">
                                <div class="products" data-product-id="">

                                    <div class="product">		
                                        <div class="product-image">
                                            <div class="image">
                                                <a href="<?php echo "../display/product-detail.php?productId=" . $value[0] . "&productName=" . $value[1]; ?>" ><img class="size-270-347" src="../assets/images/blank.gif" data-echo="<?php echo "../images/" . $value[5]; ?>" alt=""></a>
                                            </div><!-- /.image -->			


                                        </div><!-- /.product-image -->


                                        <div class="product-info text-left">
                                            <h3 class="name"><a href="<?php echo "../display/product-detail.php?productId=" . $value[0] . "&productName=" . $value[1]; ?>"><?php echo $value[1]; ?></a></h3>
                                            <div class="rating rateit-small"></div>
                                            <div class="description"></div>

                                            <div class="product-price">
                                                <span class="price"><?php echo $value[3]." VND"; ?></span>
                                                <span class="price-before-discount"></span>

                                            </div><!-- /.product-price -->

                                        </div><!-- /.product-info -->
                                        <div class="cart clearfix animate-effect">
                                            <div class="action">
                                                <ul class="list-unstyled">
                                                    <li class="add-cart-button btn-group">
                                                        <a class="add-cart" href="javascript:void(0)" data-product-id="<?php echo $value[0]; ?>">
                                                                        <button class="btn btn-primary icon"  data-toggle="dropdown" type="button">
                                                                            <i class="fa fa-shopping-cart"></i>													
                                                                        </button>
                                                                        <button class="btn btn-primary"  type="button">Add to cart</button>

                                                                    </a>
                                                    </li>

                                                    <li class="lnk wishlist">
                                                        <a class="add-to-cart" href="#" title="Wishlist">
                                                            <i class="icon fa fa-heart"></i>
                                                        </a>
                                                    </li>

                                                    <li class="lnk">
                                                        <a class="add-to-cart" href="#" title="Compare">
                                                            <i class="fa fa-retweet"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div><!-- /.action -->
                                        </div><!-- /.cart -->
                                    </div><!-- /.product -->

                                </div><!-- /.products -->
                            </div>
                            <?php
                        }
                        ?>
                    </div><!-- /.homebanner-holder -->
                    <!-- ============================================== CONTENT : END ============================================== -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /#top-banner-and-menu -->
    </body>
    <?php include '../general/footer.php'; ?>
</html>



