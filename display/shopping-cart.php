<?php
$title_page = 'Trang chủ';
include '../general/header.php';
include ('../function/getProductInfo.php');
?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <div class="breadcrumb">
            <div class="container">
                <div class="breadcrumb-inner">
                    <ul class="list-inline list-unstyled">
                        <li><a href="#">Trang chủ</a></li>
                        <li class='active'>Giỏ hàng</li>
                    </ul>
                </div><!-- /.breadcrumb-inner -->
            </div><!-- /.container -->
        </div><!-- /.breadcrumb -->

        <?php
        if (!empty($_SESSION['cart'])) {
            ?>
            <div class="body-content outer-top-xs">
                <div class="container">
                    <div class="row inner-bottom-sm">
                        <div class="shopping-cart">
                            <div class="col-md-12 col-sm-12 shopping-cart-table ">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="cart-romove item">Xoá</th>
                                                <th class="cart-description item">Hình ảnh</th>
                                                <th class="cart-product-name item">Tên sản phẩm</th>
                                                <th class="cart-qty item">Số lượng</th>
                                                <th class="cart-sub-total item">Giá sản phẩm</th>
                                                <th class="cart-total last-item">Tổng giá tiền</th>
                                            </tr>
                                        </thead><!-- /thead -->
                                        <tfoot>
                                            <tr>

                                                <td colspan="7">
                                                    <div class="shopping-cart-btn">
                                                        <span class="">
                                                            <a href="#" class="btn btn-upper btn-primary outer-left-xs">Continue Shopping</a>
                                                            <a href="../display/shopping-cart.php" class="btn btn-upper btn-primary pull-right outer-right-xs">Cập nhật giỏ hàng</a>
                                                        </span>
                                                    </div><!-- /.shopping-cart-btn -->
                                                </td>
                                            </tr>

                                        </tfoot>
                                        <tbody>
                                            <?php
                                            if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) {
                                                foreach ($_SESSION['cart'] as $key => $value) {
                                                    ?>
                                                    <tr id="cart-list">
                                                        <td class="romove-item"><a href="javascript:void(0)" title="cancel" class="icon remove-cartO" data-session-index="<?php echo $key ?>" ><i class="fa fa-trash-o"></i></a></td>
                                                        <td class="cart-image">
                                                            <a class="entry-thumbnail" href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>">
                                                                <img style="width: 154px; height: 146px" src="<?php echo "../images/" . $value[5]; ?>" alt="">
                                                            </a>
                                                        </td>
                                                        <td class="cart-product-name-info">
                                                            <h4 class='cart-product-description'><a href="<?php echo "../display/product-detail.php?productId=" . $value[0] . "&productName=" . $value[1]; ?>"><?php echo $value[1];?></a></h4>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="rating rateit-small"></div>
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <div class="reviews">
                                                                        <?php echo "(" . $value[7] . " lượt xem)"; ?>
                                                                    </div>
                                                                </div>
                                                            </div><!-- /.row -->
                                                        </td>
                                                        <td class="cart-product-quantity">
                                                            <div class="cart-quantity">
                                                                <div class="quant-input">
                                                                    <div class="arrows">
                                                                        <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
                                                                        <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
                                                                    </div>
                                                                    <input type="number" class="product-cart-quantity" data-cart-index="<?php echo $key ?>" value="<?php echo $value[9]; ?>">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="cart-product-sub-total"><span class="cart-sub-total-price"><?php echo $value[3] . " VNĐ"; ?></span></td>
                                                        <td class="cart-product-grand-total"><span class="cart-grand-total-price"><?php echo ($value[3] * $value[9]) . " VNĐ"; ?></span></td>
                                                    </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                            <p class="center-block">No product in your cart</p>
                                            <?php
                                        }
                                        ?>

                                        </tbody><!-- /tbody -->
                                    </table><!-- /table -->
                                </div>
                            </div><!-- /.shopping-cart-table -->
                            <div class="col-md-8"></div>
                            <div class="col-md-4 col-sm-12 cart-shopping-total">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>
                                                
                                                <div class="cart-grand-total">
                                                    Tổng<span class="inner-left-md"><?php echo $totalPrice." VND";?></span>
                                                </div>
                                            </th>
                                        </tr>
                                    </thead><!-- /thead -->
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="cart-checkout-btn pull-right">
                                                    <a href="../display/checkout.php"><button type="submit" class="btn btn-primary">Tiến hành đặt hàng</button></a>
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody><!-- /tbody -->
                                </table><!-- /table -->
                            </div><!-- /.cart-shopping-total -->			</div><!-- /.shopping-cart -->
                    </div> <!-- /.row -->
                    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
                    <div id="brands-carousel" class="logo-slider wow fadeInUp">

                        <h3 class="section-title">Our Brands</h3>
                        <div class="logo-slider-inner">	
                            <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                                <div class="item m-t-15">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item m-t-10">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->

                                <div class="item">
                                    <a href="#" class="image">
                                        <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
                                    </a>	
                                </div><!--/.item-->
                            </div><!-- /.owl-carousel #logo-slider -->
                        </div><!-- /.logo-slider-inner -->

                    </div><!-- /.logo-slider -->
                    <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
            </div><!-- /.body-content -->
            <?php
        }else{
            ?>
            <h1 style="text-align: center">No product in your cart</h1>
            <?php
        }
        ?>
    </body>
    <?php include '../general/footer.php'; ?>
</html>


