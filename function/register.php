<?php

include ('../function/include.php');
include ('../function/getInfoUser.php');
include ('../function/sendEmail.php');
?>

<?php

global $isSuccess;
$isSuccess = false;
$errUsername = $errPassword = $errRePassword = $errFullname = $errAddress = $errEmail = $errAgree = $errPhoneNumber = $errGender = $errDate = "";

function newUser($conn) {
    $registerUsername = $_POST['registerUsername'];
    $registerPassword = md5($_POST['registerPassword']);
    $fullname = $_POST['fullname'];
    $address = $_POST['address'];
    $email = $_POST['email'];
    $phoneNumber = $_POST['phoneNumber'];
    if ($_POST['gender'] == "male") {
        $gender = 0;
    } else {
        $gender = 1;
    }
    $date = $_POST['year'] . '-' . $_POST['month'] . '-' . $_POST['day'];
    $rand = bin2hex(openssl_random_pseudo_bytes(8));
    $query = "INSERT INTO user(`fullname`, `username`, `password`, `address`,"
            . " `gender`, `birthday`, `email`, `phone`, `status`, `verifycode`)"
            . "VALUES('$fullname','$registerUsername','$registerPassword','$address',"
            . "$gender,'$date','$email','$phoneNumber',1,'$rand')";
    $result = mysqli_query($conn, $query);
    if ($result) {
        $_SESSION['loginSession'] = $registerUsername;
        $bodyContent = "<h4>Hello $fullname <br>Congratulation you to create a new account success</h4>";
        $bodyContent .= "<p>Welcome to DCT!</p>";
        $bodyContent .= "<p>Could you please click the link below to verify that this is your email address?</p>";
        $bodyContent .= "<p><a href='http://localhost/project-php/function/verifyUser.php?username=$registerUsername&verifycode=$rand'>Click here</a></p>";
        $bodyContent .= "<p>If you need help or have any questions, please visit <a href='http://localhost/project-php/display/index.php'>Us</a></p>";
        $bodyContent .="Thanks!<br>DCT Sport";
        $bodyContent .= "<br><br><br><h1>Your verify code here:  $rand</h1>";
        $emailSubject = 'Email verify user';
        mysqli_close($conn);
        sendMail($email, $bodyContent, $emailSubject);
        header('location: ../display/index.php');
    }
    
}

function signUp($conn) {
    $isError = false;
    global $errUsername, $errPassword, $errRePassword, $errFullname, $errAddress, $errAgree, $errEmail, $errPhoneNumber, $errGender, $errDate;
    $errUsername = $errPassword = $errRePassword = $errFullname = $errAddress = $errAgree = $errEmail = $errPhoneNumber = $errGender = $errDate = "";
    if (empty($_POST['registerUsername'])) {
        $isError = true;
        $errUsername = "The username can't empty";
    }
    if (isset($_POST['registerUsername'])) {
        $username = $_POST['registerUsername'];
        $sql = "SELECT * FROM user WHERE username = '$username'";
        $result = mysqli_query($conn, $sql);
        $rows = mysqli_num_rows($result);
        if ($rows > 0) {
            $isError = true;
            $errUsername = "The username exists, please choose other username";
        }
    }
    if (empty($_POST['registerPassword'])) {
        $isError = true;
        $errPassword = "The password can't empty";
    }
    if (strlen($_POST['registerPassword']) < 8) {
        $isError = true;
        $errPassword = "The password need more 8 letters";
    }
    if (empty($_POST['rePassword'])) {
        $isError = true;
        $errRePassword = "The Re - password can't empty";
    }
    if ($_POST['rePassword'] != $_POST['registerPassword']) {
        $isError = true;
        $errRePassword = "The Re - password incorrect";
    }
    if (empty($_POST['fullname'])) {
        $isError = true;
        $errFullname = "You need to input your name";
    }
    if (empty($_POST['address'])) {
        $isError = true;
        $errAddress = "The address can't empty";
    }
    if (empty($_POST['email'])) {
        $isError = true;
        $errEmail = "The email can't empty";
    }
    if (empty($_POST['phoneNumber'])) {
        $isError = true;
        $errPhoneNumber = "The phone number can't empty";
    }
    if (empty($_POST['gender'])) {
        $isError = true;
        $errGender = "You should choose your gender";
    }

    if (empty($_POST['day']) || empty($_POST['month']) || empty($_POST['year'])) {
        $isError = true;
        $errDate = "You need to enter you date of birth";
    } else {
        $check = checkDateOfBirth((int) $_POST['day'], (int) $_POST['month'], (int) $_POST['year']);
        if (!$check) {
            $isError = true;
            $errDate = "Your date of birth incorrect";
        }
    }
    if (!isset($_POST['o-checkbox'])) {
        $isError = true;
        $errAgree = 'Please accept the terms';
    }
    if (!$isError) {
        newUser($conn);
    }
}

function checkDateOfBirth($day, $month, $year) {
    if ($day > 0 && $month > 0 && $year > 0) {
        switch ($month) {
            case 2:
                if ($year % 4 == 0 && $year % 100 == 0) {
                    if ($day < 30) {
                        $result = true;
                    } else {
                        $result = false;
                    }
                } else {
                    if ($day < 29) {
                        $result = true;
                    } else {
                        $result = false;
                    }
                }
                break;
            case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                if ($day <= 31) {
                    $result = true;
                } else {
                    $result = false;
                }
                break;
            case 2: case 4: case 6: case 9: case 11:
                if ($day <= 30) {
                    $result = true;
                } else {
                    $result = false;
                }
                break;
            default:
                $result = false;
                break;
        }
    } else {
        $result = false;
    }

    return $result;
}

if (isset($_POST['loginSubmit'])) {
    signUp($conn);
}
?>

