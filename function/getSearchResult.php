<?php
//Including Database configuration file.





include "../function/include.php";

//Getting value of "search" variable from "script.js".

if (isset($_POST['search'])) {

//Search box value assigning to $Name variable.

    $keyWords = $_POST['search'];

//Search query.

    $sql = "SELECT * FROM product WHERE MATCH(name, description)
    AGAINST('" . $keyWords . "' IN NATURAL LANGUAGE MODE) ORDER BY RAND() LIMIT 5;";

//Query execution

    $result = mysqli_query($conn, $sql);
    $rows = mysqli_num_rows($result);


//Creating unordered list to display result.

    echo '
 
<ul>
 
   ';

    //Fetching result from database.

    if ($rows > 0) {
        while ($row = $result->fetch_assoc()) {
            ?>

            <!-- Creating unordered list items.

                 Calling javascript function named as "fill" found in "script.js" file.

                 By passing fetched result as parameter. -->
            <!DOCTYPE html>
            <html>
                <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <title></title>
                    <link rel="stylesheet" href="./assets/css/images/search.css">

                </head>
                <body>

                    <div class="search" style="background: white; margin-right:0px;margin-top: 5px;">
                        <ul  style="line-height: 1.5; width: 485px" class="search">
                            <li id="search-result" onclick='fill("<?php echo $row['name']; ?>")'>

                                <a style="font-size: 13px; line-height: 1.5"href="<?php echo "../display/product-detail.php?productId=" . $row['id'] . "&productName=" . $row['name']; ?>">

                                    <!-- Assigning searched result in "Search box" in "search.php" file. -->

                                    <?php echo $row['name']; ?>


                                    <!-- Below php code is just for closing parenthesis. Don't be confused. -->

                                    <?php
                                }
                            }
                        }
                        ?>

                    </a></li>
            </ul>
        </div>
    </body>
</html>