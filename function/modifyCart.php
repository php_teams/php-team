<?php
    include ('../function/include.php');
    if(isset($_POST['cartIndex']) && isset($_POST['value']) && isset($_SESSION['cart'])){
        $cartIndex = $_POST['cartIndex'];
        $value = $_POST['value'];
        $_SESSION['cart'][$cartIndex][9] = $value;
    }
    if (count($_SESSION['cart']) > 0) {
        ?>
        <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown" id="myDropdown">
            <div class="items-cart-inner">
                <div class="total-price-basket">
                    <span class="lbl">Giỏ hàng -</span>
                    <span class="total-price">
                        <?php
                        $totalPrice = 0;
                        if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) {

                            foreach ($_SESSION['cart'] as $key => $values) {
                                $totalPrice += $values[9] * $values[3];
                            }
                        }
                        ?>
                        <span class="value"><?php echo isset($totalPrice) ? $totalPrice : 0; ?></span>
                        <span class="sign"> VND</span>
                    </span>
                </div>
                <div class="basket">
                    <i class="glyphicon glyphicon-shopping-cart"></i>
                </div>
                <div class="basket-item-count"><span class="count"><?php echo isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0; ?></span></div>

            </div>
        </a>
        <ul class="dropdown-menu dropdown">
            <li>
                <div class="cart-item product-summary">
                    <?php
                    if (isset($_SESSION['cart'])) {
                        foreach ($_SESSION['cart'] as $key => $value) {
                            ?>
                            <div class = "row">
                                <div class = "col-xs-4">
                                    <div class = "image">
                                        <a href = "../display/productDetail.php?productId=<?php echo $value[0]; ?>"><img style="width: 47px; height: 61px;" src="<?php echo "../images/" . $value[5]; ?>" alt=""></a>

                                    </div>
                                    <p>X <?php echo $value[9] ?></p>
                                </div>
                                <div class = "col-xs-7">
                                    <h3 class = "name"><a href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>"><?php echo $value[1]; ?></a></h3>
                                    <div class = "price"><?php echo $value[3] . " VNĐ"; ?></div>
                                </div>
                                <div class = "col-xs-1 action">
                                    <a href = "javascript:void(0)" class="remove-cart3" data-session-index="<?php echo $key; ?>"><i class = "fa fa-trash"></i></a>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div><!-- /.cart-item -->
                <div class="clearfix"></div>
                <hr>

                <div class="clearfix cart-total">
                    <div class="pull-right">

                        <span class="text">Tổng :</span><span class='price'>
                            <?php
                            echo isset($totalPrice) ? $totalPrice : 0;
                            ?> 
                            VNĐ
                        </span>

                    </div>
                    <div class="clearfix"></div>

                    <a href="../display/shopping-cart.php" class="btn btn-upper btn-primary btn-block m-t-20">Thanh toán</a>	
                </div><!-- /.cart-total-->
            </li>
        </ul><!-- /.dropdown-menu-->
        <script>
            $(document).ready(function () {

        //On pressing a key on "Search box" in "search.php" file. This function will be called.

                $('.remove-cart3').on("click", function () {

        //Assigning search box value to javascript variable named as "name".

                    var cid = $(this).attr("data-session-index");

        //Validating, if "name" is empty.



        //If name is not empty.



        //AJAX is called.

                    $.ajax({
                        //AJAX type is "Post".

                        type: "POST",
                        //Data will be sent to "ajax.php".

                        url: "../function/deleteCart.php",
                        //Data, that will be sent to "ajax.php".

                        data: {
                            //Assigning value of "name" into "search" variable.

                            cartId: cid

                        },
                        //If result found, this funtion will be called.
                        async: false,
                        success: function (html) {

                            //Assigning result to "display" div in "search.php" file.
                            $("#cart-box").html(html);

                        }

                    });



                });

            });
            $('.dropdown-menu').on("click.bs.dropdown", function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
        </script>
        <?php
    }
    
?>

