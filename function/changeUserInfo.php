<?php

$nameErr = $genderErr = $addressErr = $birthdayErr = $emailErr = $phoneErr = $message = "";
$username = $currentPassword = $newPassword = $sNewPassword = $cName = $cGender = $cAddress = $cBirthday = $cPhoneNumber = "";
if (isset($_POST['submitChangeInfo'])) {
    $isError = false;

    if (isset($_POST['cName'])) {
        $cName = $_POST['cName'];
    } else {
        $isError = true;
        $nameErr = "Bạn không thể để trống trường này";
    }
    if (isset($_POST['cGender'])) {
        $cGender = $_POST['cGender'] == 'male' ? 0 : 1;
    } else {
        $isError = true;
        $genderErr = "Bạn không thể để trống trường này";
    }
    if (isset($_POST['cAddress'])) {
        $cAddress = $_POST['cAddress'];
    } else {
        $isError = true;
        $addressErr = "Bạn không thể để trống trường này";
    }
    if (isset($_POST['cBirthday'])) {
        $cBirthday = $_POST['cBirthday'];
    } else {
        $isError = true;
        $birthdayErr = "Bạn không thể để trống trường này";
    }
    if (isset($_POST['cPhoneNumber'])) {
        $cPhoneNumber = $_POST['cPhoneNumber'];
    } else {
        $isError = true;
        $phoneErr = "Bạn không thể để trống trường này";
    }
    if (!$isError) {
        $sql = "UPDATE user "
                . "SET fullname = '" . $cName . "', gender = " . $cGender . ","
                . " address = '" . $cAddress . "', birthday = '" . $cBirthday . "', phone = '" . $cPhoneNumber . "'"
                . " WHERE id = " . $sId . "";


        $result = mysqli_query($conn, $sql);
        if ($result) {
            $message = "Cập nhật thành công";
            header("Refresh:0");
        } else {
            $message = "Cập nhật thất bại";
        }
    }
}
if (isset($_POST['submitChangePass'])) {
    $isError = false;

    if (isset($_POST['username'])) {
        $username = $_POST['username'];
    } else {
        $isError = true;
        $nameErr = "Bạn không thể để trống trường này";
    }
    if (isset($_POST['currentPassword'])) {
        $currentPassword = $_POST['currentPassword'];
    } else {
        $isError = true;
        $currentPasswordErr = "Bạn không thể để trống trường này";
    }
    if (isset($_POST['newPassword'])) {
        $newPassword = $_POST['newPassword'];
    } else {
        $isError = true;
        $newPasswordErr = "Bạn không thể để trống trường này";
    }
    if (isset($_POST['sNewPassword'])) {

        $sNewPassword = $_POST['sNewPassword'];
        if ($newPassword != $sNewPassword) {
            $isError = true;
            $sNewPasswordErr = "Mật khẩu xác nhận không chính xác";
        }
    } else {
        $isError = true;
        $sNewPasswordErr = "Bạn không thể để trống trường này";
    }


    if (!$isError) {
        $sql = "UPDATE user "
                . "SET password = ".$newPassword
                . " WHERE id = " . $sId . " AND password = md5('".$currentPassword."')";


        $result = mysqli_query($conn, $sql);
        if ($result) {
            $message = "Cập nhật thành công";
            header("Refresh:2");
        } else {
            $message = "Cập nhật thất bại";
        }
    }
}
?>

