<?php include ('register.php'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/css/registerform.css">
        <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
        <link rel="shortcut icon" href="../assets/images/favicon.ico">


    </head>
    <body>

        <div class="container">
            <form style="margin-left: 30px" method="post" action="">
                <div class="row">
                    <h4>Account</h4>
                    <div class="input-group input-group-icon">
                        <input type="username" placeholder="UserName" name="registerUsername"/>
                        <div class="input-icon"><i class="fa fa-user"></i></div>
                    </div>
                    <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errUsername; ?></p>
                    <div class="input-group input-group-icon">
                        <input type="password" placeholder="Password" name="registerPassword"/>
                        <div class="input-icon"><i class="fa fa-key"></i></div>
                    </div>
                    <p class="text-danger" style="font-size: 11px"><?php echo $errPassword; ?></p>
                    <div class="input-group input-group-icon">
                        <input type="password" placeholder="Re - Password" name="rePassword"/>
                        <div class="input-icon"><i class="fa fa-key"></i></div>
                    </div>
                    <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errRePassword; ?></p>
                </div>
                <div class="row">
                    <h4>Personal</h4>
                    <div class="input-group input-group-icon">
                        <input type="name" placeholder="Full Name" name="fullname"/>
                        <div class="input-icon"><i class="fa fa-smile-o"></i></div>
                    </div>
                    <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errFullname; ?></p>
                    <div class="input-group input-group-icon">
                        <input type="address" placeholder="Adress" name="address" value=""/>
                        <div class="input-icon"><i class="fa fa-bullseye"></i></div>
                    </div>
                    <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errAddress; ?></p>
                    <div class="input-group input-group-icon">
                        <input type="email" placeholder="Email Adress" name="email" value="<?php if (isset($_POST['sEmail'])) echo $_POST['sEmail']; ?>"/>
                        <div class="input-icon"><i class="fa fa-envelope-o"></i></div>
                    </div>
                    <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errEmail; ?></p>
                    <div class="input-group input-group-icon">
                        <input type="phone" placeholder="Phone Number" name="phoneNumber"/>
                        <div class="input-icon"><i class="fa fa-phone"></i></div>
                    </div>
                    <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errPhoneNumber; ?></p>
                </div>
                <div class="row">
                    <div class="col-half">
                        <h4>Date of Birth</h4>
                        <div class="input-group">
                            <div class="col-third">
                                <input type="number" placeholder="DD" name="day" maxlength="2" />
                            </div>
                            <div class="col-third">
                                <input type="number" placeholder="MM" name="month" maxlength="2" />
                            </div>
                            <div class="col-third">
                                <input type="number" placeholder="YYYY" name="year" maxlength="4" />
                            </div>
                        </div>
                    </div>
                    <div class="col-half">
                        <h4>Gender</h4>
                        <div class="input-group">
                            <input type="radio" name="gender" value="male" id="gender-male"/>
                            <label for="gender-male">Male</label>
                            <input type="radio" name="gender" value="female" id="gender-female"/>
                            <label for="gender-female">Female</label>
                        </div>
                    </div>
                </div>
                <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errDate; ?></p>
                <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errGender; ?></p>
                <div class="row">
                    <h4>Terms and Conditions</h4>
                    <div class="input-group">
                        <input type="checkbox" id="terms" name="o-checkbox"/>
                        <label for="terms">I accept the terms and conditions for signing up to this service, and hereby confirm I have read the privacy policy.</label>
                    </div>
                </div>
                <p class="text-danger" style="font-size: 11px" class="error"><?php echo $errAgree; ?></p>
                <input type="submit" name="loginSubmit" value="Sign up" class="btn-success">
            </form>
        </div>
    </body>
</html>