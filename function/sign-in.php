<?php
include ('../general/header.php');
include ('../function/login.php');
?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <div class="breadcrumb">
            <div class="container">
                <div class="breadcrumb-inner">
                    <ul class="list-inline list-unstyled">
                        <li><a href="home.html">Home</a></li>
                        <li class='active'>Authentication</li>
                    </ul>
                </div><!-- /.breadcrumb-inner -->
            </div><!-- /.container -->
        </div><!-- /.breadcrumb -->
        <div class="body-content outer-top-bd">
            <div class="container">
                <div class="sign-in-page inner-bottom-sm">
                    <div class="row">
                        <!-- Sign-in -->
                        <div class="col-md-6 col-sm-6 sign-in">
                            <h4 class="">Đăng nhập</h4>
                            <p class="">Chào mừng bạn đến với tài khoản của bạn</p>
                            <div class="social-sign-in outer-top-xs">
                                <a href="#" class="facebook-sign-in"><i class="fa fa-facebook"></i> Đăng nhập bằng Facebook</a>
                                <a href="#" class="twitter-sign-in"><i class="fa fa-twitter"></i> Đăng nhập bằng Twitter</a>
                            </div>
                            <form action="" method="post">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail1">Tài khoản <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input" id="exampleInputEmail1"
                                           name="loginUsername">
                                    <span class="help-block"></span>
                                </div>
                                <div class="form-group" ">
                                    <label class="info-title" for="exampleInputPassword1">Mật khẩu <span>*</span></label>
                                    <input type="password" class="form-control unicase-form-control text-input" id="exampleInputPassword1"
                                           name="loginPassword"  >
                                </div >
                                <span class="help-block"></span>
                                <button type="submit" class="btn-upper btn btn-primary checkout-page-button" name="loginSubmit">Login</button>
                            </form>
                            <div class="panel-group">
                                <div class="panel panel-default" style="border: 0; background-color: white">
                                    <div class="panel-heading" style="border: 0; background-color: white">
                                        <h5 class="panel-title" style="text-align: right;">
                                            <a data-toggle="collapse" href="#collapse1" class="text-danger">Lấy lại mật khẩu</a>
                                        </h5>
                                    </div>
                                    <div id="collapse1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <form action="" method="post">
                                                <h4>Nhập tài khoản bạn muốn lấy lại</h4>
                                                <div class="form-group">
                                                    <label>Tài khoản </label>
                                                    <input type="username" name="fname" value="">
                                                    <input type="submit" name="femailSubmit" value="Submit">
                                                </div>
                                            </form>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Sign-in -->
                        <!-- create a new account -->
                        <div class="col-md-6 col-sm-6 create-new-account">
                            <h4 class="checkout-subtitle">create a new account</h4>
                            <p class="text title-tag-line">Create your own DCT Sport account.</p>
                            <form class="register-form outer-top-xs" role="form" action="./sign-up.php" method="post">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputEmail2">Email Address <span>*</span></label>
                                    <input type="email" name="sEmail" class="form-control unicase-form-control text-input" id="exampleInputEmail2" >
                                </div>
                                <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Sign Up</button>
                            </form>
                            <span class="checkout-subtitle outer-top-xs">Sign Up Today And You'll Be Able To :  </span>
                            <div class="checkbox">
                                <label class="checkbox">
                                    <input type="checkbox" id="speed" value="option1" checked=""> Speed your way through the checkout.
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="track" value="option2" checked=""> Track your orders easily.
                                </label>
                                <label class="checkbox">
                                    <input type="checkbox" id="keep" value="option3" checked=""> Keep a record of all your purchases.
                                </label>
                            </div>
                        </div>
                        <!-- create a new account -->			</div><!-- /.row -->
                </div><!-- /.sigin-in-->
                <!-- ============================================== BRANDS CAROUSEL ============================================== -->
                <div id="brands-carousel" class="logo-slider wow fadeInUp">
                    <h3 class="section-title">Our Brands</h3>
                    <div class="logo-slider-inner">
                        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
                            <div class="item m-t-15">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand1.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item m-t-10">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand2.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand3.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand4.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand5.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand6.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand2.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand4.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand1.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                            <div class="item">
                                <a href="#" class="image">
                                    <img data-echo="../assets/images/brands/brand5.png" src="../assets/images/blank.gif" alt="">
                                </a>
                            </div><!--/.item-->
                        </div><!-- /.owl-carousel #logo-slider -->
                    </div><!-- /.logo-slider-inner -->
                </div><!-- /.logo-slider -->
                <!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
        </div><!-- /.body-content -->
        <?php include('../general/footer.php'); ?>
    </body>
</html>