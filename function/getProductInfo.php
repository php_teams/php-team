<?php
function getListProductInfo($conn, $sql) {
    $result = mysqli_query($conn, $sql);
    $rows = mysqli_num_rows($result);
    $resulsP = array();
    if ($rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $pId = $row["id"];
            $pName = $row["name"];
            $pQuantity = $row["quantity"];
            $pPrice = $row["price"];
            $pDescription = $row["description"];
            $pImage = $row["image"];
            $test = array();
            $test[] = $pId;
            $test[] = $pName;
            $test[] = $pQuantity;
            $test[] = $pPrice;
            $test[] = $pDescription;
            $test[] = $pImage;
            $resulsP[] = $test;
        }
    }
    return $resulsP;
}


function getProductInfo($conn, $proId) {
    $sql = "SELECT * FROM product WHERE id = $proId";
    $result = mysqli_query($conn, $sql);
    $rows = mysqli_num_rows($result);
    $proInfo = array();
    if ($rows == 1) {
        while ($row = $result->fetch_assoc()) {
            $pId = $row["id"];
            $pName = $row["name"];
            $pQuantity = $row["quantity"];
            $pPrice = $row["price"];
            $pDescription = $row["description"];
            $pImage = $row["image"];
            $pCategoryId = $row["category_id"];
            $pViews= $row["views"];
            $pFavorites = $row["favorites"];
            $proInfo[] = $pId;
            $proInfo[] = $pName;
            $proInfo[] = $pQuantity;
            $proInfo[] = $pPrice;
            $proInfo[] = $pDescription;
            $proInfo[] = $pImage;
            $proInfo[] = $pCategoryId;
            $proInfo[] = $pViews;
            $proInfo[] = $pFavorites;
            
        }
    }
    return $proInfo;
}
?>

