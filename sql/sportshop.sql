
DROP DATABASE IF EXISTS sportshop;
CREATE DATABASE sportshop;
USE sportshop;

DROP TABLE IF EXISTS user;
CREATE TABLE user (
    id INT(11) AUTO_INCREMENT,
    fullname VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    address VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
    gender TINYINT(1),
    birthday DATE,
    email VARCHAR(255) COLLATE utf8_unicode_ci NOT NULL,
    phone VARCHAR(50) NOT NULL,
    status TINYINT(1),
    verifyCode VARCHAR(16) NULL,
    PRIMARY KEY (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS category;
CREATE TABLE category (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50),
    parentid INT(11) NOT NULL,
    description TEXT COLLATE utf8_unicode_ci
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE product (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) COLLATE utf8_unicode_ci,
    quantity INT,
    status VARCHAR(50),
    dayin DATETIME,
    price FLOAT(11) NOT NULL,
    is_active TINYINT(1) DEFAULT 1,
    description TEXT,
    image VARCHAR(255),
    favorites INT NOT NULL DEFAULT 0,
    views INT NOT NULL DEFAULT 0,
    category_id INT(11),
    FOREIGN KEY (category_id)
        REFERENCES category (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE product
ADD FULLTEXT(name, description);

CREATE TABLE `images` (
    `id` INT(11) AUTO_INCREMENT,
    `product_id` INT(11) NOT NULL,
    `path` VARCHAR(255) NOT NULL,
    `is_thumbnail` TINYINT(1),
    `is_active` TINYINT(1),
    PRIMARY KEY (`id`),
    FOREIGN KEY (`product_id`)
        REFERENCES `product` (`id`)
)  ENGINE=MYISAM DEFAULT CHARSET=UTF8;

CREATE TABLE `feedback` (
    `id` INT(11) AUTO_INCREMENT,
    `summary` INT(11) NULL,
    `message` VARCHAR(255) NOT NULL,
    `time` DATETIME NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `review` TINYINT(1) NULL,
    `productId` INT(11) NOT NULL,
    FOREIGN KEY (`productId`)
        REFERENCES `product` (`id`),
    PRIMARY KEY(id)
)  ENGINE=MYISAM DEFAULT CHARSET=UTF8;

CREATE TABLE `wishlist` (
    `id` INT(11) AUTO_INCREMENT,
    `userId` INT(11) NOT NULL,
    `productId` INT(11) NOT NULL,
    FOREIGN KEY (`userId`)
        REFERENCES `user` (`id`),
    FOREIGN KEY (`productId`)
        REFERENCES `product` (`id`),
    PRIMARY KEY(id)
)  ENGINE=MYISAM DEFAULT CHARSET=UTF8;


DROP TABLE IF EXISTS orders;
CREATE TABLE orders (
    id INT(11) PRIMARY KEY AUTO_INCREMENT,
    userid INT,
    date DATE,
    process VARCHAR(50) COLLATE utf8_unicode_ci NOT NULL,
    FOREIGN KEY (userid)
        REFERENCES user (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


DROP TABLE IF EXISTS orders_detail;
CREATE TABLE order_detail (
    id INT(11) PRIMARY KEY,
    quantity INT,
    sale FLOAT,
    orderid INT,
    productid INT,
    FOREIGN KEY (orderid)
        REFERENCES orders (id),
    FOREIGN KEY (productid)
        REFERENCES product (id)
)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Before*/
/*/////////////////////////////////////////////////////////////////////////////////*/


INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (1,'Trang phục thể thao',0,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (2,'Thiết bị tập',0,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (3,'Tennis',0,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (4,'Golf',0,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (5,'Bóng đá',0,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (6,'Bơi lội',0,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (7,'Giày thể thao',1,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (8,'Áo thể thao',1,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (9,'Quần thể thao',1,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (10,'Phụ kiện',1,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (11,'Giày running nam',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (12,'Giày training nam',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (13,'Giày sportwear nam',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (14,'Dép thể thao nam',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (15,'Giày running nữ',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (16,'Giày training nữ',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (17,'Giày sportwear nữ',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (18,'Dép thể thao nữ',7,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (19,'Áo running nam',8,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (20,'Áo training nam',8,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (21,'Áo sportwear nam',8,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (22,'Áo running nữ',8,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (23,'Áo training nữ',8,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (24,'Áo sportwear nữ',8,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (25,'Quần running nam',9,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (26,'Quần training nam',9,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (27,'Quần sportwear nam',9,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (28,'Quần running nữ',9,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (29,'Quần training nữ',9,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (30,'Quần sportwear nữ',9,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (31,'Tất vớ thể thao',10,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (32,'Túi xách - vali - balo',10,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (33,'Mũ thể thao',10,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (34,'Bình nước thể thao',10,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (35,'Phụ kiện khác',10,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (36,'Thiết bị tập Cybex',2,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (37,'Máy tập chạy bộ',2,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (38,'Xe đạp',2,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (39,'Giàn tạ, máy tập tạ',2,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (40,'Thiết bị tập khác',2,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (41,'Giày tennis',3,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (42,'Quần áo tennis',3,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (43,'Vợt tennis',3,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (44,'Phụ kiện tennis',3,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (45,'Giày golf',4,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (46,'Quần áo Golf',4,NULL);
INSERT INTO `category` (`id`,`name`,`parentid`,`description`) VALUES (47,'Dụng cụ',4,NULL);

/*After*/




/*Start insert product*/

/*insert giay running nam id = 11*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giầy running adidas ALPHABOUNCE EM nam BY1',100,'offer',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nam/1.jpg',11),
('Giầy running adidas ALPHABOUNCE EM nam BY2',100,'offer',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nam/2.jpg',11),
('Giầy running adidas ALPHABOUNCE EM nam BY3',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nam/3.jpg',11),
('Giầy running adidas ALPHABOUNCE EM nam BY4',100,'Hàng mới nhập',NOW(),1500000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nam/4.jpg',11),
('Giầy running adidas ALPHABOUNCE EM nam BY5',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nam/1.jpg',11);

/*insert giay training nam id = 12*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giầy training adidas ALPHABOUNCE EM nam BY1',100,'offer',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nam/1.jpg',12),
('Giầy training adidas ALPHABOUNCE EM nam BY2',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nam/2.jpg',12),
('Giầy training adidas ALPHABOUNCE EM nam BY3',100,'sale',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nam/3.jpg',12),
('Giầy training adidas ALPHABOUNCE EM nam BY4',100,'sale',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nam/4.jpg',12),
('Giầy training adidas ALPHABOUNCE EM nam BY5',100,'sale',NOW(),1100000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nam/1.jpg',12);



/*insert giay sportwear nam id = 13*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giầy sportwear adidas ALPHABOUNCE EM nam BY1',100,'offer',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nam/1.jpg',13),
('Giầy sportwear adidas ALPHABOUNCE EM nam BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nam/2.jpg',13),
('Giầy sportwear adidas ALPHABOUNCE EM nam BY3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nam/3.jpg',13),
('Giầy sportwear adidas ALPHABOUNCE EM nam BY4',100,'sale',NOW(),500000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nam/4.jpg',13),
('Giầy sportwear adidas ALPHABOUNCE EM nam BY5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nam/1.jpg',13);

/*insert dep the thao nam id = 14*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Dép thể thao ALPHABOUNCE EM nam BY1',100,'offer',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nam/1.jpg',14),
('Dép thể thao ALPHABOUNCE EM nam BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nam/2.jpg',14),
('Dép thể thao ALPHABOUNCE EM nam BY3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nam/3.jpg',14),
('Dép thể thao ALPHABOUNCE EM nam BY4',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nam/4.jpg',14),
('Dép thể thao ALPHABOUNCE EM nam BY5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nam/1.jpg',14);

/*insert giay running nu id = 15*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giầy running adidas ALPHABOUNCE EM nữ BY1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nu/1.jpg',15),
('Giầy running adidas ALPHABOUNCE EM nữ BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nu/2.jpg',15),
('Giầy running adidas ALPHABOUNCE EM nữ BY3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nu/3.jpg',15),
('Giầy running adidas ALPHABOUNCE EM nữ BY4',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nu/4.jpg',15),
('Giầy running adidas ALPHABOUNCE EM nữ BY5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/giay_the_thao/giay_running_nu/1.jpg',15);

/*insert giay training nu id = 16*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giầy training adidas ALPHABOUNCE EM nữ BY1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nu/1.jpg',16),
('Giầy training adidas ALPHABOUNCE EM nữ BY2',100,'offer',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nu/2.jpg',16),
('Giầy training adidas ALPHABOUNCE EM nữ BY3',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nu/3.jpg',16),
('Giầy training adidas ALPHABOUNCE EM nữ BY4',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nu/4.jpg',16),
('Giầy training adidas ALPHABOUNCE EM nữ BY5',100,'Hàng mới nhập',NOW(),1100000,'','trang_phuc_the_thao/giay_the_thao/giay_training_nu/1.jpg',16);



/*insert giay sportwear nu id = 17*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giầy sportwear adidas ALPHABOUNCE EM nữ BY1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nu/1.jpg',17),
('Giầy sportwear adidas ALPHABOUNCE EM nữ BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nu/2.jpg',17),
('Giầy sportwear adidas ALPHABOUNCE EM nữ BY3',100,'sale',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nu/3.jpg',17),
('Giầy sportwear adidas ALPHABOUNCE EM nữ BY4',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nu/4.jpg',17),
('Giầy sportwear adidas ALPHABOUNCE EM nữ BY5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/giay_the_thao/giay_sportwear_nu/1.jpg',17);

/*insert dep the thao nu id = 18*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Dép adidas RESPONSE GRAPHIC TEE Nam AI1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nu/1.jpg',18),
('Dép adidas RESPONSE GRAPHIC TEE Nam AI2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nu/2.jpg',18),
('Dép adidas RESPONSE GRAPHIC TEE Nam AI3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nu/3.jpg',18),
('Dép adidas RESPONSE GRAPHIC TEE Nam AI4',100,'sale',NOW(),500000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nu/4.jpg',18),
('Dép adidas RESPONSE GRAPHIC TEE Nam AI5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/giay_the_thao/dep_the_thao_nu/1.jpg',18);

/*insert ao running nam id = 19*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo running adidas ALPHABOUNCE EM nam BY1',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nam/1.jpg',19),
('Áo running adidas ALPHABOUNCE EM nam BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nam/2.jpg',19),
('Áo running adidas ALPHABOUNCE EM nam BY3',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nam/3.jpg',19),
('Áo running adidas ALPHABOUNCE EM nam BY4',100,'Hàng mới nhập',NOW(),1500000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nam/4.jpg',19),
('Áo running adidas ALPHABOUNCE EM nam BY5',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nam/1.jpg',19);

/*insert ao training nam id = 20*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo training adidas ALPHABOUNCE EM nam BY1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nam/1.jpg',20),
('Áo training adidas ALPHABOUNCE EM nam BY2',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nam/2.jpg',20),
('Áo training adidas ALPHABOUNCE EM nam BY3',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nam/3.jpg',20),
('Áo training adidas ALPHABOUNCE EM nam BY4',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nam/4.jpg',20),
('Áo training adidas ALPHABOUNCE EM nam BY5',100,'Hàng mới nhập',NOW(),1100000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nam/1.jpg',20);



/*insert ao sportwear nam id = 21*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo sportwear adidas ALPHABOUNCE EM nam BY1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nam/1.jpg',21),
('Áo sportwear adidas ALPHABOUNCE EM nam BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nam/2.jpg',21),
('Áo sportwear adidas ALPHABOUNCE EM nam BY3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nam/3.jpg',21),
('Áo sportwear adidas ALPHABOUNCE EM nam BY4',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nam/4.jpg',21),
('Áo sportwear adidas ALPHABOUNCE EM nam BY5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nam/1.jpg',21);

/*insert ao running nu id = 22*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo running adidas ALPHABOUNCE EM nữ BY1',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nu/1.jpg',22),
('Áo running adidas ALPHABOUNCE EM nữ BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nu/2.jpg',22),
('Áo running adidas ALPHABOUNCE EM nữ BY3',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nu/3.jpg',22),
('Áo running adidas ALPHABOUNCE EM nữ BY4',100,'Hàng mới nhập',NOW(),1500000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nu/4.jpg',22),
('Áo running adidas ALPHABOUNCE EM nữ BY5',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/ao_the_thao/ao_running_nu/1.jpg',22);

/*insert ao training nu id = 23*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo training adidas ALPHABOUNCE EM nữ BY1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nu/1.jpg',23),
('Áo training adidas ALPHABOUNCE EM nữ BY2',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nu/2.jpg',23),
('Áo training adidas ALPHABOUNCE EM nữ BY3',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nu/3.jpg',23),
('Áo training adidas ALPHABOUNCE EM nữ BY4',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nu/4.jpg',23),
('Áo training adidas ALPHABOUNCE EM nữ BY5',100,'Hàng mới nhập',NOW(),1100000,'','trang_phuc_the_thao/ao_the_thao/ao_training_nu/1.jpg',23);



/*insert ao sportwear nu id = 24*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo sportwear adidas ALPHABOUNCE EM nữ BY1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nu/1.jpg',24),
('Áo sportwear adidas ALPHABOUNCE EM nữ BY2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nu/2.jpg',24),
('Áo sportwear adidas ALPHABOUNCE EM nữ BY3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nu/3.jpg',24),
('Áo sportwear adidas ALPHABOUNCE EM nữ BY4',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nu/4.jpg',24),
('Áo sportwear adidas ALPHABOUNCE EM nữ BY5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/ao_the_thao/ao_sportwear_nu/1.jpg',24);


/*insert quan running nam id = 25*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Quần running adidas ALPHABOUNCE EM nam BK1',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nam/1.jpg',25),
('Quần running adidas ALPHABOUNCE EM nam BK2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nam/2.jpg',25),
('Quần running adidas ALPHABOUNCE EM nam BK3',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nam/3.jpg',25),
('Quần running adidas ALPHABOUNCE EM nam BK4',100,'Hàng mới nhập',NOW(),1500000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nam/4.jpg',25),
('Quần running adidas ALPHABOUNCE EM nam BK5',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nam/1.jpg',25);

/*insert quan training nam id = 26*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Quần training adidas ALPHABOUNCE EM nam BK1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nam/1.jpg',26),
('Quần training adidas ALPHABOUNCE EM nam BK2',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nam/2.jpg',26),
('Quần training adidas ALPHABOUNCE EM nam BK3',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nam/3.jpg',26),
('Quần training adidas ALPHABOUNCE EM nam BK4',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nam/4.jpg',26),
('Quần training adidas ALPHABOUNCE EM nam BK5',100,'Hàng mới nhập',NOW(),1100000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nam/1.jpg',26);



/*insert quan sportwear nam id = 27*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Quần sportwear adidas ALPHABOUNCE EM nam BK1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nam/1.jpg',27),
('Quần sportwear adidas ALPHABOUNCE EM nam BK2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nam/2.jpg',27),
('Quần sportwear adidas ALPHABOUNCE EM nam BK3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nam/3.jpg',27),
('Quần sportwear adidas ALPHABOUNCE EM nam BK4',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nam/4.jpg',27),
('Quần sportwear adidas ALPHABOUNCE EM nam BK5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nam/1.jpg',27);

/*insert quan running nu id = 28*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Quần running adidas ALPHABOUNCE EM nữ BK1',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nu/1.jpg',28),
('Quần running adidas ALPHABOUNCE EM nữ BK2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nu/2.jpg',28),
('Quần running adidas ALPHABOUNCE EM nữ BK3',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nu/3.jpg',28),
('Quần running adidas ALPHABOUNCE EM nữ BK4',100,'Hàng mới nhập',NOW(),1500000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nu/4.jpg',28),
('Quần running adidas ALPHABOUNCE EM nữ BK5',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/quan_the_thao/quan_running_nu/1.jpg',28);

/*insert quan training nu id = 29*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Quần training adidas ALPHABOUNCE EM nữ BK1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nu/1.jpg',29),
('Quần training adidas ALPHABOUNCE EM nữ BK2',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nu/2.jpg',29),
('Quần training adidas ALPHABOUNCE EM nữ BK3',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nu/3.jpg',29),
('Quần training adidas ALPHABOUNCE EM nữ BK4',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nu/4.jpg',29),
('Quần training adidas ALPHABOUNCE EM nữ BK5',100,'Hàng mới nhập',NOW(),1100000,'','trang_phuc_the_thao/quan_the_thao/quan_training_nu/1.jpg',29);



/*insert quan sportwear nu id = 30*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Quần sportwear adidas ALPHABOUNCE EM nữ BK1',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nu/1.jpg',30),
('Quần sportwear adidas ALPHABOUNCE EM nữ BK2',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nu/2.jpg',30),
('Quần sportwear adidas ALPHABOUNCE EM nữ BK3',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nu/3.jpg',30),
('Quần sportwear adidas ALPHABOUNCE EM nữ BK4',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nu/4.jpg',30),
('Quần sportwear adidas ALPHABOUNCE EM nữ BK5',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/quan_the_thao/quan_sportwear_nu/1.jpg',30);

/*insert quan tat vo the thao id = 31*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Tất Running NIKE nam SX4750-01',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/phu_kien/tat_vo_the_thao/1.jpg',31),
('Tất Running NIKE nam SX4750-02',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/phu_kien/tat_vo_the_thao/2.jpg',31),
('Tất Running NIKE nam SX4750-03',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/phu_kien/tat_vo_the_thao/3.jpg',31),
('Tất Running NIKE nam SX4750-04',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/phu_kien/tat_vo_the_thao/4.jpg',31),
('Tất Running NIKE nam SX4750-05',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/phu_kien/tat_vo_the_thao/1.jpg',31);

/*insert tui xach vali balo id = 32*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Ba Lô Thể Thao Adidas S22101',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/phu_kien/tuixach_vali_balo/1.jpg',32),
('Ba Lô Thể Thao Adidas S22102',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/phu_kien/tuixach_vali_balo/2.jpg',32),
('Ba Lô Thể Thao Adidas S22103',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/phu_kien/tuixach_vali_balo/3.jpg',32),
('Ba Lô Thể Thao Adidas S22104',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/phu_kien/tuixach_vali_balo/4.jpg',32),
('Ba Lô Thể Thao Adidas S22105',100,'Hàng mới nhập',NOW(),600000,'','trang_phuc_the_thao/phu_kien/tuixach_vali_balo/1.jpg',32);

/*insert mu the thao id = 33*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Mũ running nike AROBILL 827616-01',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/phu_kien/mu_the_thao/1.jpg',33),
('Mũ running nike AROBILL 827616-02',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/phu_kien/mu_the_thao/2.jpg',33),
('Mũ running nike AROBILL 827616-03',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/phu_kien/mu_the_thao/3.jpg',33),
('Mũ running nike AROBILL 827616-04',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/phu_kien/mu_the_thao/4.jpg',33);

/*insert binh nuoc id = 34*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Bình nước Reebok 650mm - vàng',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/phu_kien/binh_nuoc_the_thao/1.jpg',34);

/*insert thiet bi tap khac id = 35*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Băng Cánh Tay Nike Speed Performance Arm Bands – 2.5''',100,'Hàng mới nhập',NOW(),190000,'','trang_phuc_the_thao/phu_kien/phu_kien_khac/1.jpg',35),
('Băng Cánh Tay Nike Speed Performance Arm Bands – 2.5''',100,'Hàng mới nhập',NOW(),290000,'','trang_phuc_the_thao/phu_kien/phu_kien_khac/2.jpg',35),
('Băng Cánh Tay Nike Speed Performance Arm Bands – 2.5''',100,'Hàng mới nhập',NOW(),400000,'','trang_phuc_the_thao/phu_kien/phu_kien_khac/3.jpg',35),
('Băng Cánh Tay Nike Speed Performance Arm Bands – 2.5''',100,'Hàng mới nhập',NOW(),500000,'','trang_phuc_the_thao/phu_kien/phu_kien_khac/4.jpg',35);

/*insert thiet bi tap cybex id = 36*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giàn tạ tập cơ đùi Cybex 11040-01',100,'Hàng mới nhập',NOW(),190000,'','thiet_bi_tap/thiet_bi_tap_cybex/1.jpg',36),
('Giàn tạ tập cơ đùi Cybex 11040-02',100,'Hàng mới nhập',NOW(),290000,'','thiet_bi_tap/thiet_bi_tap_cybex/2.jpg',36),
('Giàn tạ tập cơ đùi Cybex 11040-03',100,'Hàng mới nhập',NOW(),400000,'','thiet_bi_tap/thiet_bi_tap_cybex/3.jpg',36),
('Giàn tạ tập cơ đùi Cybex 11040-04',100,'Hàng mới nhập',NOW(),500000,'','thiet_bi_tap/thiet_bi_tap_cybex/4.jpg',36);

/*insert may tap chay bo id = 37*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Máy chạy bộ đa chức năng SS - 01 SP',100,'Hàng mới nhập',NOW(),190000,'','thiet_bi_tap/may_tap_chay_bo/1.jpg',37),
('Máy chạy bộ đa chức năng SS - 02 SP',100,'Hàng mới nhập',NOW(),290000,'','thiet_bi_tap/may_tap_chay_bo/2.jpg',37),
('Máy chạy bộ đa chức năng SS - 03 SP',100,'Hàng mới nhập',NOW(),400000,'','thiet_bi_tap/may_tap_chay_bo/3.jpg',37),
('Máy chạy bộ đa chức năng SS - 04 SP',100,'Hàng mới nhập',NOW(),500000,'','thiet_bi_tap/may_tap_chay_bo/4.jpg',37);

/*insert xe dap id = 38*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Xe đạp tập cao cấp Sport Life 1A',100,'Hàng mới nhập',NOW(),190000,'','thiet_bi_tap/xe_dap/1.jpg',38),
('Xe đạp tập cao cấp Sport Life 2A',100,'Hàng mới nhập',NOW(),290000,'','thiet_bi_tap/xe_dap/2.jpg',38),
('Xe đạp tập cao cấp Sport Life 3A',100,'Hàng mới nhập',NOW(),400000,'','thiet_bi_tap/xe_dap/3.jpg',38),
('Xe đạp tập cao cấp Sport Life 4A',100,'Hàng mới nhập',NOW(),500000,'','thiet_bi_tap/xe_dap/4.jpg',38);

/*insert dan ta id = 39*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Ghế Tạ Đa Năng mạ Niken 601502N',100,'Hàng mới nhập',NOW(),190000,'','thiet_bi_tap/gian_ta/1.jpg',39),
('Ghế Tạ Đa Năng mạ Niken 601502N',100,'Hàng mới nhập',NOW(),290000,'','thiet_bi_tap/gian_ta/2.jpg',39),
('Ghế Tạ Đa Năng mạ Niken 601502N',100,'Hàng mới nhập',NOW(),400000,'','thiet_bi_tap/gian_ta/3.jpg',39),
('Ghế Tạ Đa Năng mạ Niken 601502N',100,'Hàng mới nhập',NOW(),500000,'','thiet_bi_tap/gian_ta/4.jpg',39);

/*insert theit bi tap khac id = 40*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Băng cổ tay 1',100,'Hàng mới nhập',NOW(),190000,'','thiet_bi_tap/thiet_bi_tap_khac/1.jpg',40),
('Băng cổ tay 2',100,'Hàng mới nhập',NOW(),290000,'','thiet_bi_tap/thiet_bi_tap_khac/2.jpg',40),
('Băng cổ tay 3',100,'Hàng mới nhập',NOW(),400000,'','thiet_bi_tap/thiet_bi_tap_khac/3.jpg',40),
('Băng cổ tay 4',100,'Hàng mới nhập',NOW(),500000,'','thiet_bi_tap/thiet_bi_tap_khac/4.jpg',40);

/*insert giay tennis id = 41*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giày Tennis adidas Barricade Aspire Stripes Nữ A1',100,'Hàng mới nhập',NOW(),190000,'','tennis/giay_tennis/1.jpg',41),
('Giày Tennis adidas Barricade Aspire Stripes Nữ A2',100,'Hàng mới nhập',NOW(),290000,'','tennis/giay_tennis/2.jpg',41),
('Giày Tennis adidas Barricade Aspire Stripes Nữ A3',100,'Hàng mới nhập',NOW(),400000,'','tennis/giay_tennis/3.jpg',41),
('Giày Tennis adidas Barricade Aspire Stripes Nữ A4',100,'Hàng mới nhập',NOW(),500000,'','tennis/giay_tennis/4.jpg',41);

/*insert quan ao tennis id = 42*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo Tennis adidas AEROKNIT POLO SHIRT 1',100,'Hàng mới nhập',NOW(),190000,'','tennis/quan_ao_tennis/1.jpg',42),
('Áo Tennis adidas AEROKNIT POLO SHIRT 2',100,'Hàng mới nhập',NOW(),290000,'','tennis/quan_ao_tennis/2.jpg',42),
('Áo Tennis adidas AEROKNIT POLO SHIRT 3',100,'Hàng mới nhập',NOW(),400000,'','tennis/quan_ao_tennis/3.jpg',42),
('Áo Tennis adidas AEROKNIT POLO SHIRT 4',100,'Hàng mới nhập',NOW(),500000,'','tennis/quan_ao_tennis/4.jpg',42);

/*insert vot tennis id = 43*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Vợt Tennis Babolat PURE DRIVE JUNIOR 24 1',100,'Hàng mới nhập',NOW(),190000,'','tennis/vot_tennis/1.jpg',43),
('Vợt Tennis Babolat PURE DRIVE JUNIOR 24 2',100,'Hàng mới nhập',NOW(),290000,'','tennis/vot_tennis/2.jpg',43),
('Vợt Tennis Babolat PURE DRIVE JUNIOR 24 3',100,'Hàng mới nhập',NOW(),400000,'','tennis/vot_tennis/3.jpg',43),
('Vợt Tennis Babolat PURE DRIVE JUNIOR 24 4',100,'Hàng mới nhập',NOW(),500000,'','tennis/vot_tennis/4.jpg',43);

/*insert phu kien tennis id = 44*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Túi Vợt Babolat Racket Driver X6 751106',100,'Hàng mới nhập',NOW(),190000,'','tennis/phu_kien_tennis/1.jpg',44),
('Túi Vợt Babolat Racket Driver X6 123123',100,'Hàng mới nhập',NOW(),290000,'','tennis/phu_kien_tennis/2.jpg',44),
('Túi Vợt Babolat Racket Driver X6 122122',100,'Hàng mới nhập',NOW(),400000,'','tennis/phu_kien_tennis/3.jpg',44),
('Túi Vợt Babolat Racket Driver X6 122423',100,'Hàng mới nhập',NOW(),500000,'','tennis/phu_kien_tennis/4.jpg',44);

/*insert giay golf id = 45*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Giày Golf adidas Barricade Aspire Stripes1',100,'Hàng mới nhập',NOW(),190000,'','golf/giay_golf/1.jpg',45),
('Giày Golf adidas Barricade Aspire Stripes2',100,'Hàng mới nhập',NOW(),290000,'','golf/giay_golf/2.jpg',45),
('Giày Golf adidas Barricade Aspire Stripes3',100,'Hàng mới nhập',NOW(),400000,'','golf/giay_golf/3.jpg',45),
('Giày Golf adidas Barricade Aspire Stripes4',100,'Hàng mới nhập',NOW(),500000,'','golf/giay_golf/4.jpg',45);


/*insert quan ao golf id = 46*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Áo Golf AEROKNIT POLO SHIRT1',100,'Hàng mới nhập',NOW(),190000,'','golf/quan_ao_golf/1.jpg',46),
('Áo Golf AEROKNIT POLO SHIRT2',100,'Hàng mới nhập',NOW(),290000,'','golf/quan_ao_golf/2.jpg',46),
('Áo Golf AEROKNIT POLO SHIRT3',100,'Hàng mới nhập',NOW(),400000,'','golf/quan_ao_golf/3.jpg',46),
('Áo Golf AEROKNIT POLO SHIRT4',100,'Hàng mới nhập',NOW(),500000,'','golf/quan_ao_golf/4.jpg',46);

/*insert dung cu golf id = 47*/
INSERT INTO product(`name`,`quantity`,`status`,`dayin`,`price`,`description`,`image`,`category_id`)
VALUES
('Bóng Golf - TM10 PENTA TP DZ1',100,'Hàng mới nhập',NOW(),190000,'','golf/dung_cu/1.jpg',47),
('Bóng Golf - TM10 PENTA TP DZ2',100,'Hàng mới nhập',NOW(),290000,'','golf/dung_cu/2.jpg',47),
('Bóng Golf - TM10 PENTA TP DZ3',100,'Hàng mới nhập',NOW(),400000,'','golf/dung_cu/3.jpg',47),
('Bóng Golf - TM10 PENTA TP DZ4',100,'Hàng mới nhập',NOW(),500000,'','golf/dung_cu/4.jpg',47);







/*End of insert product*/ 