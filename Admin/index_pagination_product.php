<?php include '../function/include.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Bootstrap Example</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h2>PRODUCT</h2>

            <?php
//            session_start();
            if (isset($_SESSION['message']) && $_SESSION['message'] != '') {

                echo $_SESSION['message'] . "<br>";
                unset($_SESSION['message']);
            }
            ?>
            <a class="btn btn-primary" href="./create_product.php">Create</a>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>Day in</th>     
                        <th>Price</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Favorites</th>
                        <th>Views</th>
                        <th>Category ID</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $rows_result = $conn->query("SELECT id FROM product");
                    $rows_no = $rows_result->num_rows;
                    $rows_per_page = 3;
                    $pages_no = intval(($rows_no - 1) / $rows_per_page) + 1;

                    $page_curent = isset($_GET['p']) ? $_GET['p'] : 1;
                    if (!$page_curent)
                        $page_curent = 1;
                    $start = ($page_curent - 1) * $rows_per_page;


                    $sql = "SELECT * FROM product limit $start,$rows_per_page";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>

                                <td><?php echo $row['id'] ?></td>
                                <td><?php echo $row['name'] ?></td>            
                                <td><?php echo $row['quantity'] ?></td>
                                <td><?php echo $row['status'] ?></td>
                                <td><?php echo $row['dayin'] ?></td>
                                <td><?php echo $row['price'] ?></td>
                                <td><?php echo $row['description'] ?></td>
                                <td><img src="../images/<?php echo $row['image'] ?>" width="100" height="100"></td>
                                <td><?php echo $row['favorites'] ?></td>
                                <td><?php echo $row['views'] ?></td>
                                <td><?php echo $row['category_id'] ?></td>

                                <td>  <a href="./edit_product.php?id=<?php echo $row['id'] ?>">Edit</a><span>|</span> <button onclick="deleteme(<?php echo $row['id'] ?>)">Delete</button>
                                </td>
                            </tr>  


                            <?php
                        }
                    } else {
                        echo "0 results";
                    }
                    ?>


                </tbody>
            </table>
            <?php
            if ($pages_no > 1) {
                echo "Pages: ";
                if ($page_curent > 1) {
                    echo "<a href='index_pagination_product.php?p=1' class=\"page\" >First</a>&nbsp;&nbsp;";
                    echo "<a href='index_pagination_product.php?p=" . ($page_curent - 1) . "' class=\"page\">Previous&nbsp;&nbsp;";
                }
                echo "<b class=\"page\" >$page_curent</b>&nbsp;&nbsp;";
                if ($page_curent < $pages_no) {
                    echo "<a href='index_pagination_product.php?p=" . ($page_curent + 1) . "' class=\"page\" >Next&nbsp;&nbsp;";
                    echo "<a href='index_pagination_product.php?p=$pages_no' class=\"page\" >Last</a>&nbsp;&nbsp;";
                }
            }
            ?>	
        </div>

    </body>
</body>

<script type="text/javascript">
    
    function deleteme(delid){
        if(confirm("Do you want to delete this product?")){
            window.location.href='./delete.php?id='+delid+'';
            return true;
        }
    }

</script>

</html>


