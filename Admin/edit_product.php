<?php include '../function/include.php'; ?>

<?php 
//define variables and set to empty values
$nameErr = $quantityErr= $statusErr= $dayinErr = $priceErr =$category_idErr ="";
$name = $quantity = $status = $dayin = $price = $category_id ="";
$isError = false;
$user_id=0;
if(isset($_GET['id'])&& $_GET['id'] !=''){
    $stmt = $conn->prepare("SELECT * FROM product WHERE id = ?");
    //echo "Error updating record: ".$conn->error;
    $stmt->bind_param('i', $_GET['id']);
    
    if($stmt->execute()){
       $result = $stmt->get_result();
       while ($row=$result->fetch_assoc()){
           //result is in row
           $user_id = $row['id'];
           $name = $row['name'];
           $quantity = $row['quantity'];
           $status = $row['status'];
           $dayin = $row['dayin'];
           $price = $row['price'];
           $category_id = $row['category_id'];
           
       }
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $user_id=0;
    $message='';
    if(!empty($_POST["user_id"])){
        $user_id = $_POST["user_id"];
    }
    
    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
        $isError = true;
    } else {
        $name = test_input($_POST["name"]);
    }

    if (empty($_POST["quantity"])) {
        $titleErr = "Quantity is required";
        $isError = true;
    } else {
        $quantity = test_input($_POST["quantity"]);
    }
    
    if (empty($_POST["status"])) {
        $statusErr = "Status is required";
        $isError = true;
    } else {
        $status = test_input($_POST["status"]);
    }
    
    if (empty($_POST["dayin"])) {
        $dayin = "Dayin is required";
        $isError = true;
    } else {
        $dayin = test_input($_POST["dayin"]);
    }
    
    if (empty($_POST["price"])) {
        $priceErr = "Price is required";
        $isError = true;
    } else {
        $price = test_input($_POST["price"]);
    }
    
    if (empty($_POST["category_id"])) {
        $category_idErr = "Category_id is required";
        $isError = true;
    } else {
        $category_id = test_input($_POST["category_id"]);
    }
    
    
    
    // UPDATE EMPLOYEE INFO 
    if(!$isError){
        $sql = "UPDATE product SET name='$name', quantity ='$quantity', status='$status',dayin='$dayin', price='$price', category_id='$category_id' WHERE id=$user_id";
        echo $sql;
        if($conn->query($sql)===TRUE){
            $message = "Record updated succesfully";
        } else {
            echo "Error updating record: ".$conn->error;
        }
    }
}

function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

        <div class="container">
            <h2>Edit Employees</h2>
            
            <?php 
              if( isset($message) && $message != '') {
              ?>
              <p><span class="error"><?php echo $message; ?></span></p>
             <?php
              }
            ?>
            <p><span class="error">* required field.</span></p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                Name: <input type="text" name="name" value="<?php echo "$name" ?>">
                <span class=" error">* <?php echo $nameErr; ?></span>
                <br><br>
                Quantity: <input type="text" name="quantity" value="<?php echo "$quantity" ?>">
                <span class=" error"><?php echo $quantityErr; ?></span>
                <br><br>
                Status: <input type="text" name="status" value="<?php echo "$status" ?>">
                <span class=" error"><?php echo $statusErr; ?></span>        
                <br><br>
                Day in: <input type="text" name="dayin" value="<?php echo "$dayin" ?>">
                <span class=" error"><?php echo $dayinErr; ?></span>        
                <br><br>
                Price: <input type="text" name="price" value="<?php echo "$price" ?>">
                <span class=" error">* <?php echo $priceErr; ?></span>
                <br><br>
                Category_id: <input type="text" name="category_id" value="<?php echo "$category_id" ?>">
                <span class=" error"><?php echo $category_idErr; ?></span>        
                <br><br>
                
                <input type="hidden" name="user_id" value="<?php echo $user_id ?>"/>
                <a class="btn btn-default" href="./index_pagination.php">Back</a>
                <input type="submit" name="submit" value="Submit">  
            </form>

        </div>

    </body>
</html>

