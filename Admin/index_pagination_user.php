<?php include '../function/include.php'; ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <h2>users</h2>

            <?php
            session_start();
            if (isset($_SESSION['message']) && $_SESSION['message'] != '') {

                echo $_SESSION['message'] . "<br>";
                unset($_SESSION['message']);
            }
            ?>
            <a class="btn btn-primary" href="./create.php">Create</a>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fullname</th>
                        <th>Username</th>
                        <th>Address</th>
                        <th>Gender</th>
                        <th>Birthday</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

<?php
$rows_result = $conn->query("SELECT id FROM user");
$rows_no = $rows_result->num_rows;
$rows_per_page = 3;
$pages_no = intval(($rows_no - 1) / $rows_per_page) + 1;

$page_curent = isset($_GET['p']) ? $_GET['p'] : 1;
if (!$page_curent)
    $page_curent = 1;
$start = ($page_curent - 1) * $rows_per_page;


$sql = "SELECT * FROM user limit $start,$rows_per_page";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while ($row = $result->fetch_assoc()) {
        ?>
                            <tr>
                                <td><?php echo $row['id'] ?></td>
                                <td><?php echo $row['fullname'] ?></td>
                                <td><?php echo $row['username'] ?></td>
                                <td><?php echo $row['address'] ?></td>
                                <td><?php echo $row['gender'] ?></td>
                                <td><?php echo $row['birthday'] ?></td>
                                <td><?php echo $row['email'] ?></td>
                                <td><?php echo $row['phone'] ?></td>
                                <td><?php echo $row['status'] ?></td>                               
                            </tr>  


                            <?php
                        }
                    } else {
                        echo "0 results";
                    }
                    ?>


                </tbody>
            </table>
            <?php
            if ($pages_no > 1) {
                echo "Pages: ";
                if ($page_curent > 1) {
                    echo "<a href='index_pagination.php?p=1' class=\"page\" >First</a>&nbsp;&nbsp;";
                    echo "<a href='index_pagination.php?p=" . ($page_curent - 1) . "' class=\"page\">Previous&nbsp;&nbsp;";
                }
                echo "<b class=\"page\" >$page_curent</b>&nbsp;&nbsp;";
                if ($page_curent < $pages_no) {
                    echo "<a href='index_pagination.php?p=" . ($page_curent + 1) . "' class=\"page\" >Next&nbsp;&nbsp;";
                    echo "<a href='index_pagination.php?p=$pages_no' class=\"page\" >Last</a>&nbsp;&nbsp;";
                }
            }
            ?>	
        </div>

    </body>
</body>
</html>


