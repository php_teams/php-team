<?php include '../function/include.php'; ?> 

<?php
//session_start();
// define variables and set to empty values
$nameErr = $quantityErr = $statusErr = $dayinErr = $priceErr = $descriptionErr = $imageErr = $favoritesErr = $veiwsErr = $category_idErr = "";
$name = $quantity = $status = $dayin = $price = $description = $image = $favorites = $veiws = $category_id = "";
$isError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $image = $_FILES['imaged']['names'];
    $img_link = "images/" . $image;
    move_uploaded_file($_FILES['imaged']['tmp_name'], $img_link);

    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
        $isError = true;
    } else {
        $name = test_input($_POST["name"]);
    }

    if (empty($_POST["quantity"])) {
        $titleErr = "Quantity is required";
        $isError = true;
    } else {
        $quantity = test_input($_POST["quantity"]);
    }

    if (empty($_POST["status"])) {
        $statusErr = "Description is required";
        $isError = true;
    } else {
        $status = test_input($_POST["status"]);
    }

    if (empty($_POST["dayin"])) {
        $dayinErr = "Category_id is required";
        $isError = true;
    } else {
        $dayin = test_input($_POST["dayin"]);
    }

    if (empty($_POST["price"])) {
        $priceErr = "Price is required";
        $isError = true;
    } else {
        $price = test_input($_POST["price"]);
    }

    if (empty($_POST["description"])) {
        $descriptionErr = "Description is required";
        $isError = true;
    } else {
        $description = test_input($_POST["description"]);
    }

    if (empty($_POST["image"])) {
        $imageErr = "Image is required";
        $isError = true;
    } else {
//        $image = test_input($_POST["image"]);
        $image = $_FILES['imaged']['names'];
    }

    if (empty($_POST["favorites"])) {
        $favoritesErr = "Favorite is required";
        $isError = true;
    } else {
        $favorites = test_input($_POST["favorites"]);
    }

    if (empty($_POST["veiws"])) {
        $veiwsErr = "Views is required";
        $isError = true;
    } else {
        $veiws = test_input($_POST["veiws"]);
    }

    if (empty($_POST["category_id"])) {
        $category_idErr = "Category_id is required";
        $isError = true;
    } else {
        $category_id = test_input($_POST["category_id"]);
    }

    // Update imployee info
    if (!$isError) {
        $sql = " INSERT INTO product (name, quantity, status, dayin, price,description,image,favorites,veiws,category_id) VALUES(?, ?, ?,?,?,?,?,?,?,?) ";
        //prepare and bind
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sississiii", $name, $quantity, $status, $dayin, $price, $description, $image, $favorites, $veiws, $category_id);

        if ($stmt->execute() === TRUE) {
            $_SESSION['message'] = "Record was create successfully";
            header("Location: index_pagination_product.php"); /* Redirect browser */
            exit();
        } else {
            echo "Error update record: " . $stmt->error;
        }
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css">

    </head>
    <body>

        <div class="container">
            <h2>Create Employees</h2>
            <p><span class="error">* required field.</span></p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                Name: <input type="text" name="name">
                <span class=" error">* <?php echo $nameErr; ?></span>
                <br><br>
                Quantity: <input type="text" name="quantity">
                <span class=" error"><?php echo $quantityErr; ?></span>
                <br><br>
                Status: <input type="text" name="status">
                <span class=" error"><?php echo $statusErr; ?></span>
                <br><br>
                Day in: <input type="datetime" name="dayin">
                <span class=" error"><?php echo $dayinErr; ?></span>        
                <br><br>
                Price: <input type="text" name="price">
                <span class=" error">* <?php echo $priceErr; ?></span>
                <br><br>
                Description: <input type="text" name="description">
                <span class=" error">* <?php echo $descriptionErr; ?></span>
                <br><br>
                Image: <input type="file" name="image">
                <span class=" error">* <?php echo $imageErr; ?></span>
                <br><br>
                Favorites: <input type="text" name="favorites">
                <span class=" error">* <?php echo $favoritesErr; ?></span>
                <br><br>
                Veiws: <input type="text" name="veiws">
                <span class=" error">* <?php echo $veiwsErr; ?></span>
                <br><br>
                Category_id: <input type="text" name="category_id">
                <span class=" error"><?php echo $category_idErr; ?></span>        
                <br><br>
                <a class="btn btn-default" href="./index_pagination_product.php">Back</a>
                <input type="submit" name="submit" value="Submit">  
            </form>

        </div>

    </body>
</html>





