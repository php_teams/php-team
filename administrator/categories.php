<?php
include '../function/include.php';
//include 'header.php';
//include 'product-action.php';
?>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Bootstrap Example</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
<div style="background-color: #999999; height: 6%;">
    <div class="col-lg-6" style="text-align: center; padding: 3px">
        <div class="col-md-6" style="text-align: center"><a class="btn btn-primary" href="./index.php">HOME</a></div>
    </div>
    <div class="col-lg-6" style="text-align: center; padding: 3px">
        <a class="btn btn-primary" href="./add-category.php">Create</a> 
    </div>
</div>

<!--<div class="row-fluid">
    <div class="span12">
        <ul class="breadcrumb">
            <li><a href="./index.php"><i class="icon-home" style="font-size: 18px; width: 30px;"></i></a><span class="divider">&nbsp;</span></li>
            <li><a href="#">Sản phẩm</a><span class="divider-last">&nbsp;</span></li>
        </ul>
    </div>
</div>-->



<div id="page" class="dashboard">
        <table class="table"    >
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Name</th>
                        <th>Parent ID</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $rows_result = $conn->query("SELECT id FROM category");
                    $rows_no = $rows_result->num_rows;
                    $rows_per_page = 7;
                    $pages_no = intval(($rows_no - 1) / $rows_per_page) + 1;

                    $page_curent = isset($_GET['p']) ? $_GET['p'] : 1;
                    if (!$page_curent)
                        $page_curent = 1;
                    $start = ($page_curent - 1) * $rows_per_page;


                    $sql = "SELECT * FROM category limit $start,$rows_per_page";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        // output data of each row
                        while ($row = $result->fetch_assoc()) {
                            ?>
                            <tr>

                                <td><?php echo $row['id'] ?></td>
                                <td><?php echo $row['name'] ?></td>            
                                <td><?php echo $row['parentid'] ?></td>
                                <td><?php echo $row['description'] ?></td>  
                                <td> <span>|</span> <button onclick="deleteme(<?php echo $row['id'] ?>)">Delete</button>
                                </td>
                            </tr>  


                            <?php
                        }
                    } else {
                        echo "0 results";
                    }
                    ?>


                </tbody>
            </table>
            <?php
            if ($pages_no > 1) {
                echo "Pages: ";
                if ($page_curent > 1) {
                    echo "<a href='categories.php?p=1' class=\"page\" >First</a>&nbsp;&nbsp;";
                    echo "<a href='categories.php?p=" . ($page_curent - 1) . "' class=\"page\">Previous&nbsp;&nbsp;";
                }
                echo "<b class=\"page\" >$page_curent</b>&nbsp;&nbsp;";
                if ($page_curent < $pages_no) {
                    echo "<a href='categories.php?p=" . ($page_curent + 1) . "' class=\"page\" >Next&nbsp;&nbsp;";
                    echo "<a href='categories.php?p=$pages_no' class=\"page\" >Last</a>&nbsp;&nbsp;";
                }   
            }
            ?>	
        </div>
    </body>
</html>
<script type="text/javascript">
    
    function deleteme(delid){
        if(confirm("Do you want to delete this product?")){
            window.location.href='./delete-categories.php?id='+delid+'';
            return true;
        }
    }

</script>

<?php
include 'footer_add_product.php';
?>