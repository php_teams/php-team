<?php
include '../function/include.php';
include '../function/getcategories.php';
?>
<?php
$nameErr = $parent_idErr = $descriptionErr = "";
$name = $parent_id = $description = "";
$isError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
        $isError = true;
    } else {
        $name = test_input($_POST["name"]);
    }

    if (empty($_POST["parentid"])) {
        $parent_idErr = "Parent_id is required";
        $isError = true;
    } else {
        $parent_id = test_input($_POST["parentid"]);
    }

    if (empty($_POST["description"])) {
        $descriptionErr = "Description is required";
        $isError = true;
    } else {
        $description = test_input($_POST["description"]);
    }

    // Update imployee info
    if (!$isError) {
        $sql = " INSERT INTO category (name, parentid, description) VALUES(?,?,?) ";
        //prepare and bind
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sis", $name, $parent_id, $description);

        if ($stmt->execute() === TRUE) {
            $_SESSION['message'] = "Record was create successfully";
            header("Location: categories.php"); /* Redirect browser */
            exit();
        } else {
            echo "Error update record: " . $stmt->error;
        }
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Bootstrap Example</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div style="background-color: #999999; height: 10%;padding-top: 1%   ">
            <div class="row">

                <div class="col-md-6" style="text-align: center"><a class="btn btn-primary" href="./index.php">HOME</a></div>
                <div class="col-md-6" style="text-align: center; color: #cc0000"><marquee>CREATE CATEGORY</marquee></div>

            </div>

        </div>

        <div class="container">
            <h1 style="color: #0055CC; font-size: 30px">Thêm danh mục sản phẩm</h1>
            <p><span class="error">* required field.</span></p>
            <form method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                <div class="form-group">
                    <label class="control-label col-sm-2" >Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder=" Enter name's your product" name="name">
                        <span class=" error">* <?php echo $nameErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" >Parent Id:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder=" Enter your parent id of product" name="parentid">
                        <span class=" error">* <?php echo $parent_idErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" >Description:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder=" Enter information about it" name="description">
                        <span class=" error">* <?php echo $descriptionErr; ?></span>
                    </div>
                </div>
                
                <div class="form-group"> 
                    <div class="col-sm-2">
                        
                    </div>
                    <div class="col-sm-5">
                        <a class="btn btn-primary" href="./categories.php">Back</a>
                      
                    </div>
                    <div class="col-sm-5">
                       
                        <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>

            </form>

        </div>
        <script></script>
    </body>
</html>
<?php
include 'footer_add_product.php';
?>
