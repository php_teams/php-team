<?php include '../function/include.php'; ?> 

<?php
// define variables and set to empty values
$fullnameErr = $usernameErr = $passwordErr = $addressErr = $genderErr = $birthdayErr = $emailErr = $phoneErr = $statusErr = $verifyCodeErr ="";
$fullname = $username = $password = $address= $gender= $birthday= $email= $phone= $status= $verifyCode="";
$isError = false;

if($_SERVER["REQUEST_METHOD"]=="POST"){
    if(empty($_POST["fullname"])){
        $fullnameErr = "Fullname is required";
        $isError = true;
    } else {
        $fullname = test_input($_POST["fullname"]);
    }
    
    if (empty($_POST["username"])) {
        $usernameErr = "Username is required";
        $isError = true;
    } else {
        $username = test_input($_POST["username"]);
    }
    
    if (empty($_POST["password"])) {
        $passwordErr = "Password is required";
        $isError = true;
    } else {
        $password = test_input($_POST["password"]);
    }
    
    if (empty($_POST["address"])) {
        $addressErr = "Address is required";
        $isError = true;
    } else {
        $address = test_input($_POST["address"]);
    }
    
    if (empty($_POST["gender"])) {
        $genderErr = "Gender is required";
        $isError = true;
    } else {
        $gender = test_input($_POST["gender"]);
    }
    
    if (empty($_POST["birthday"])) {
        $birthdayErr = "Birthday is required";
        $isError = true;
    } else {
        $birthday = test_input($_POST["birthday"]);
    }
    
    if (empty($_POST["email"])) {
        $emailErr = "Email is required";
        $isError = true;
    } else {
        $email = test_input($_POST["email"]);
    }
    
    if (empty($_POST["phone"])) {
        $phoneErr = "Phone is required";
        $isError = true;
    } else {
        $phone = test_input($_POST["phone"]);
    }
    
    if (empty($_POST["status"])) {
        $statusErr = "Status is required";
        $isError = true;
    } else {
        $status = test_input($_POST["status"]);
    }
    
    if (empty($_POST["verifyCode"])) {
        $verifyCodeErr = "verifyCode is required";
        $isError = true;
    } else {
        $verifyCode = test_input($_POST["verifyCode"]);
    }
    
    
    // Update imployee info
    if(!$isError){
        $sql = " INSERT INTO user (fullname, username, password, address, gender, birthday, email, phone,status,verifyCode) VALUES(?, ?, ?,?, ?, ?, ?, ?, ?,?) ";
        //prepare and bind
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("ssssisssis", $fullname, $username, md5($password), $address, $gender, $birthday, $email, $phone, $status,$verifyCode);
        
        if($stmt->execute() === TRUE){
            $_SESSION['message'] = "Record was create successfully";
             header("Location: index_pagination_user.php"); /* Redirect browser */
             exit();
        } else {
            echo "Error update record: ". $stmt->error;
        }
        
    }
}

function test_input($data){
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<html lang="en">
    <head>
        <title>Bootstrap Example</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style.css">

    </head>
    <body>

        <div class="container">
            <h2>Create User</h2>
            <p><span class="error">* required field.</span></p>
            <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                Fullname: <input type="text" name="fullname">
                <span class=" error">* <?php echo $fullnameErr; ?></span>
                <br><br>
                Username: <input type="text" name="username">
                <span class=" error">* <?php echo $usernameErr; ?></span>
                <br><br>
                Password: <input type="text" name="password">
                <span class=" error">* <?php echo $passwordErr; ?></span>
                <br><br>
                Address: <input type="text" name="address">
                <span class=" error">* <?php echo $addressErr; ?></span>
                <br><br>
                Gender: <input type="text" name="gender">
                <span class=" error">* <?php echo $genderErr; ?></span>
                <br><br>
                Birthday: <input type="date" name="birthday">
                <span class=" error">* <?php echo $birthdayErr; ?></span>
                <br><br>
                Email: <input type="text" name="email">
                <span class=" error">* <?php echo $emailErr; ?></span>
                <br><br>
                Phone: <input type="text" name="phone">
                <span class=" error">* <?php echo $phoneErr; ?></span>
                <br><br>
                Status: <input type="text" name="status">
                <span class=" error">* <?php echo $statusErr; ?></span>
                <br><br>
                Vericode: <input type="text" name="verifyCode">
                <span class=" error">* <?php echo $verifyCodeErr; ?></span>
                <br><br>
                
                <a class="btn btn-default" href="./index_pagination.php">Back</a>
                <input type="submit" name="submit" value="Submit">  
            </form>

        </div>

    </body>
</html>





