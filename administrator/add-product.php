<?php
include '../function/include.php';
include '../function/getcategories.php';
$_SESSION['task'] = 'products';
//include 'header_add_product.php';
//include 'action.php';
//include 'footer.php';
?>
<?php
$nameErr = $quantityErr = $statusErr = $dayinErr = $priceErr = $descriptionErr = $imageErr = $favoritesErr = $viewsErr = $category_idErr = "";
$name = $quantity = $status = $dayin = $price = $description = $image = $favorites = $views = $category_id = "";
$isError = false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

//    $image = $_FILES['imaged']['names'];
//    $img_link = "images/" . $image;
//    move_uploaded_file($_FILES['imaged']['tmp_name'], $img_link);
    if (isset($_POST['submit'])) {
        if ($_FILES['image']['name'] == !NULL) {
//        if($_FILES['image']['type'] == "image/jpg"
//        && $_FILES['image']['type'] == "image/png"
//        && $_FILES['image']['type'] == "image/gif"){
            if ($_FILES['image']['size'] > 1048576) {
                echo "File không được lớn hơn 1mb";
            } else {
                $path = "../images/";
                $tmp_name = $_FILES['image']['tmp_name'];
                $image_name = $_FILES['image']['name'];
                $type = $_FILES['image']['type'];
                $size = $_FILES['image']['size'];
                $image = $image_name;

                move_uploaded_file($tmp_name, $path . $image_name);
                echo "file uploaded";
            }
//        } else {
//            echo "Kiểu file không hợp lệ";
//        }
        } else {
            echo "Vui lòng chọn file";
        }
    }

    if (empty($_POST["name"])) {
        $nameErr = "Name is required";
        $isError = true;
    } else {
        $name = test_input($_POST["name"]);
    }

    if (empty($_POST["quantity"])) {
        $titleErr = "Quantity is required";
        $isError = true;
    } else {
        $quantity = test_input($_POST["quantity"]);
    }

    if (empty($_POST["status"])) {
        $statusErr = "Status is required";
        $isError = true;
    } else {
        $status = test_input($_POST["status"]);
    }

    if (empty($_POST["dayin"])) {
        $dayinErr = "Day in is required";
        $isError = true;
    } else {
        $dayin = test_input($_POST["dayin"]);
    }

    if (empty($_POST["price"])) {
        $priceErr = "Price is required";
        $isError = true;
    } else {
        $price = test_input($_POST["price"]);
    }

    if (empty($_POST["description"])) {
        $descriptionErr = "Description is required";
        $isError = true;
    } else {
        $description = test_input($_POST["description"]);
    }

    if (empty($_POST["image"])) {
        $imageErr = "Image is required";
        $isError = FALSE;
    } else {
//        $image = test_input($_POST["image"]);
        $image = test_input($_POST["image"]);
    }

    if (empty($_POST["favorites"])) {
        $favoritesErr = "Favorite is required";
        $isError = true;
    } else {
        $favorites = test_input($_POST["favorites"]);
    }

    if (empty($_POST["views"])) {
        $viewsErr = "Views is required";
        $isError = true;
    } else {
        $views = test_input($_POST["views"]);
    }

    if (empty($_POST["category_id"])) {
        $category_idErr = "Category_id is required";
        $isError = true;
    } else {
        $category_id = test_input($_POST["category_id"]);
    }

    // Update imployee info
    if (!$isError) {
        $sql = " INSERT INTO product (name, quantity, status, dayin, price,description,image,favorites,views,category_id) VALUES(?, ?, ?,?,?,?,?,?,?,?) ";
        //prepare and bind
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sississiii", $name, $quantity, $status, $dayin, $price, $description, $image, $favorites, $views, $category_id);

        if ($stmt->execute() === TRUE) {
            $_SESSION['message'] = "Record was create successfully";
            header("Location: products.php"); /* Redirect browser */
            exit();
        } else {
            echo "Error update record: " . $stmt->error;
        }
    }
}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Bootstrap Example</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div style="background-color: #999999; height: 10%;padding-top: 1%   ">
            <div class="row">

                <div class="col-md-6" style="text-align: center"><a class="btn btn-primary" href="./index.php">HOME</a></div>
                <div class="col-md-6" style="text-align: center; color: #cc0000"><marquee>CREATE PRODUCT</marquee></div>

            </div>

        </div>

        <div class="container">
            <h1 style="color: #0055CC; font-size: 30px">Thêm sản phẩm</h1>
            <p><span class="error">* required field.</span></p>
            <form method="post" enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>">  
                <div class="form-group">
                    <label class="control-label col-sm-2" >Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder=" Enter name's your product" name="name">
                        <span class=" error">* <?php echo $nameErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" >Quantity:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder=" Enter quantity" name="quantity">
                        <span class=" error">* <?php echo $quantityErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" >Status:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" placeholder=" Enter status" name="status">
                        <span class=" error">* <?php echo $statusErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" >Day in:</label>
                    <div class="col-sm-10">
                        <input type="datetime" class="form-control" name="dayin">
                        <span class=" error">* <?php echo $dayinErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" > Price:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="price">
                        <span class=" error">* <?php echo $priceErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" > Description:</label>
                    <div class="col-sm-10">
                        <input type="textarea" class="form-control" name="description">
                        <span class=" error">* <?php echo $descriptionErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" > Image:</label>
                    <div class="col-sm-10">
                        <input type="file" name="image">
                        <span class=" error">* <?php echo $imageErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" > Favorites:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="favorites">
                        <span class=" error">* <?php echo $favoritesErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" > Views:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="views">
                        <span class=" error">* <?php echo $viewsErr; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2" > Category Id:</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="category_id">
                        <span class=" error">* <?php echo $category_idErr; ?></span>
                    </div>
                </div>

                <div class="form-group"> 
                    <div class="col-sm-2">
                        
                    </div>
                    <div class="col-sm-5">
                        <a class="btn btn-primary" href="./products.php">Back</a>
                      
                    </div>
                    <div class="col-sm-5">
                       
                        <button type="submit" name="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <script></script>
    </body>
</html>
<?php
include 'footer_add_product.php';
?>
