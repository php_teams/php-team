<?php
include ('../function/include.php');
include ('../function/getInfoUser.php');
include ('../function/getcategories.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <!-- Meta -->
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="MediaCenter, Template, eCommerce">
        <meta name="robots" content="all">
        <title><?php echo $title_page ?></title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">

        <!-- Customizable CSS -->
        <link rel="stylesheet" href="../assets/css/main.css">
        <link rel="stylesheet" href="../assets/css/green.css">
        <link rel="stylesheet" href="../assets/css/owl.carousel.css">
        <link rel="stylesheet" href="../assets/css/owl.transitions.css">
        <!--<link rel="stylesheet" href="../assets/css/owl.theme.css">-->
        <link href="../assets/css/lightbox.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/css/animate.min.css">
        <link rel="stylesheet" href="../assets/css/rateit.css">
        <link rel="stylesheet" href="../assets/css/bootstrap-select.min.css">

        <!-- Demo Purpose Only. Should be removed in production -->
        <link rel="stylesheet" href="../assets/css/config.css">

        <link href="../assets/css/green.css" rel="alternate stylesheet" title="Green color">
        <link href="../assets/css/blue.css" rel="alternate stylesheet" title="Blue color">
        <link href="../assets/css/red.css" rel="alternate stylesheet" title="Red color">
        <link href="../assets/css/orange.css" rel="alternate stylesheet" title="Orange color">
        <link href="../assets/css/dark-green.css" rel="alternate stylesheet" title="Darkgreen color">
        <!-- Demo Purpose Only. Should be removed in production : END -->


        <!-- Icons/Glyphs -->
        <link rel="stylesheet" href="../assets/css/font-awesome.min.css">

        <!-- Fonts --> 
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Encode+Sans+Expanded' rel='stylesheet' type='text/css'>

        <!-- Favicon -->
        <link rel="shortcut icon" href="../assets/images/favicon.ico">

        <!-- HTML5 elements and media queries Support for IE8 : HTML5 shim and Respond.js -->
        <!--[if lt IE 9]>
                <script src="../assets/js/html5shiv.js"></script>
                <script src="../assets/js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <header class="header-style-1">
            <!-- ============================================== TOP MENU ============================================== -->
            <div class="top-bar animate-dropdown">
                <div class="container">
                    <div class="header-top-inner">
                        <div class="cnt-account">
                            <ul class="list-unstyled">
                                <?php
                                if (isset($_SESSION['loginSession'])) {
                                    echo '<li><a href="../display/accountInfo.php"><i class="icon fa fa-user"></i>Tài khoản</a></li>'
                                    . '<li><a href="#"><i class="icon fa fa-heart"></i>Danh sách yêu thích</a></li>'
                                    . '<li/><a href="../function/logout.php"><i class="icon fa fa-sign-in"></i>Đăng xuất</a></li>';
                                } else {
                                    echo '<li/><a href="../function/sign-up.php"><i class="icon fa fa-sign-in"></i>Đăng kí</a></li>';
                                    echo '<li/><a href="../function/sign-in.php"><i class="icon fa fa-user"></i>Đăng nhập</a></li>';
                                }
                                ?>
                                <li><a href="../display/shopping-cart.php"><i class="icon fa fa-shopping-cart"></i>Giỏ hàng của tôi</a></li>

                            </ul>

                            <div class="clearfix"></div>
                        </div><!-- /.header-top-inner -->
                    </div><!-- /.container -->
                </div><!-- /.header-top -->
                <!-- ============================================== TOP MENU : END ============================================== -->
                <div class="main-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                                <!-- ============================================================= LOGO ============================================================= -->
                                <div class="logo">
                                    <a href="../display/index.php">

                                        <img src="../assets/images/logo.png" alt="">

                                    </a>
                                </div><!-- /.logo -->
                                <!-- ============================================================= LOGO : END ============================================================= -->				</div><!-- /.logo-holder -->

                            <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder">
                                <div class="contact-row">
                                    <div class="phone inline">
                                        <i class="icon fa fa-phone"></i> (+84) 166 252 438
                                    </div>
                                    <div class="contact inline">
                                        <i class="icon fa fa-envelope"></i> DCTSportShop@gmail.com
                                    </div>
                                </div><!-- /.contact-row -->
                                <!-- ============================================================= SEARCH AREA ============================================================= -->
                                <div class="search-area">
                                    <form id="js-search-panel" class="search-panel" action="../display/search-result.php" >
                                        <div class="control-group">

                                            <ul class="categories-filter animate-dropdown">
                                                <li class="dropdown">

                                                    <a class="dropdown-toggle"  data-toggle="dropdown" href="category.html">Categories <b class="caret"></b></a>

                                                    <ul class="dropdown-menu" role="menu" >
                                                        <li class="menu-header">Football</li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="category.html">-  Clother</a></li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="category.html">-Shoes </a></li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="category.html">- Club </a></li>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" href="category.html">- Tearm</a></li>
                                                    </ul>
                                                </li>
                                            </ul>

                                            <input class="search-field" name="search" id="search" placeholder="Nhập tên sản phẩm để tìm kiếm" />
                                            <a class="search-button" href="#" ></a>    

                                        </div>
                                        <div id="display" style="display: block;position: absolute;">

                                        </div>
                                    </form>
                                </div><!-- /.search-area -->
                                <!-- ============================================================= SEARCH AREA : END ============================================================= -->				</div><!-- /.top-search-holder -->

                            <div class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row">
                                <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->

                                <div class="dropdown dropdown-cart cart-box" id="cart-box">
                                    <a href="#" class="dropdown-toggle lnk-cart" data-toggle="dropdown" id="myDropdown">
                                        <div class="items-cart-inner">
                                            <div class="total-price-basket">
                                                <span class="lbl">Giỏ hàng -</span>
                                                <span class="total-price">
                                                    <?php
                                                    $totalPrice = 0;
                                                    if (isset($_SESSION['cart']) && count($_SESSION['cart']) > 0) {

                                                        foreach ($_SESSION['cart'] as $key => $values) {
                                                            $totalPrice += $values[9] * $values[3];
                                                        }
                                                    }
                                                    ?>
                                                    <span class="value"><?php echo isset($totalPrice) ? $totalPrice : 0; ?></span>
                                                    <span class="sign"> VND</span>
                                                </span>
                                            </div>
                                            <div class="basket">
                                                <i class="glyphicon glyphicon-shopping-cart"></i>
                                            </div>
                                            <div class="basket-item-count"><span class="count"><?php echo isset($_SESSION['cart']) ? count($_SESSION['cart']) : 0; ?></span></div>

                                        </div>
                                    </a>
                                    <ul class="dropdown-menu dropdown">
                                        <li>
                                            <div class="cart-item product-summary">
                                                <?php
                                                if (isset($_SESSION['cart'])) {
                                                    foreach ($_SESSION['cart'] as $key => $value) {
                                                        ?>
                                                        <div class = "row">
                                                            <div class = "col-xs-4">
                                                                <div class = "image">
                                                                    <a href = "../display/productDetail.php?productId=<?php echo $value[0]; ?>"><img style="width: 47px; height: 61px;" src="<?php echo "../images/" . $value[5]; ?>" alt=""></a>

                                                                </div>
                                                                <p>X <?php echo $value[9] ?></p>
                                                            </div>
                                                            <div class = "col-xs-7">

                                                                <h3 class = "name"><a href="../display/product-detail.php?productId=<?php echo $value[0] . "&productName=" . $value[1] ?>"><?php echo $value[1]; ?></a></h3>
                                                                <div class = "price"><?php echo $value[3] . " VNĐ"; ?></div>
                                                            </div>
                                                            <div class = "col-xs-1 action">
                                                                <a href = "javascript:void(0)" class="remove-cart" data-session-index="<?php echo $key; ?>"><i class = "fa fa-trash"></i></a>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div><!-- /.cart-item -->
                                            <div class="clearfix"></div>
                                            <hr>

                                            <div class="clearfix cart-total">
                                                <div class="pull-right">

                                                    <span class="text">Tổng :</span><span class='price'>
                                                        <?php
                                                        echo isset($totalPrice) ? $totalPrice : 0;
                                                        ?> 
                                                        VNĐ
                                                    </span>

                                                </div>
                                                <div class="clearfix"></div>

                                                <a href="../display/shopping-cart.php" class="btn btn-upper btn-primary btn-block m-t-20">Thanh toán</a>	
                                            </div><!-- /.cart-total-->


                                        </li>
                                    </ul><!-- /.dropdown-menu-->
                                </div><!-- /.dropdown-cart -->

                                <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= -->				</div><!-- /.top-cart-row -->
                        </div><!-- /.row -->

                    </div><!-- /.container -->

                </div><!-- /.main-header -->

                <!-- ============================================== NAVBAR ============================================== -->
                <div class="header-nav animate-dropdown">
                    <div class="container">
                        <div class="yamm navbar navbar-default" role="navigation">
                            <div class="navbar-header">
                                <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                            <div class="nav-bg-class">
                                <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                                    <div class="nav-outer">
                                        <ul class="nav navbar-nav">
                                            <li class="dropdown">
                                                <a href="../display/index.php">Trang chủ</a>
                                            </li>
                                            <?php
                                            $_SESSION['categories'] = array();

                                            $sql = "SELECT * FROM category WHERE parentid = 0";
                                            $mainCategory = array();
                                            $mainCategory = getListCategories($conn, $sql);

                                            foreach ($mainCategory as $key => $value) {

                                                $sql = "SELECT * FROM category WHERE parentid = $value[0]";
                                                $subCategory = array();
                                                $subCategory = getListCategories($conn, $sql);
                                                if (count($subCategory) > 0) {
                                                    $listSubcategorys = array();
                                                    ?>

                                                    <li class="active dropdown yamm-fw">
                                                        <a href="<?php echo "../display/showProductByCategory.php?categoryName=" . $value[1]; ?>" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown"><?php echo $value[1] ?></a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <div class="yamm-content">
                                                                    <div class="row">
                                                                        <div class="col-md-12 col-sm-12">
                                                                            <div class="row">
                                                                                <div class='col-md-12'>
                                                                                    <?php
                                                                                    foreach ($subCategory as $key => $sub) {

                                                                                        $sql = "SELECT * FROM category WHERE parentid = $sub[0]";
                                                                                        $subCategory = array();
                                                                                        $miniSubCategory = getListCategories($conn, $sql);
                                                                                        if (count($miniSubCategory) > 0) {
                                                                                            $listMiniSubs = array();
                                                                                        } else {
                                                                                            
                                                                                        }
                                                                                        ?>
                                                                                        <div class="col-xs-12 col-sm-12 <?php echo count($miniSubCategory) > 0 ? 'col-md-3' : 'col-md-12'; ?>">
                                                                                            <a href="<?php echo "../display/showProductByCategory.php?categoryId=" . $value[0] . "&subCategoryId=" . $sub[0] . "&categoryName=" . $sub[1]; ?>"><h2 class="title"><?php echo $sub[1] ?></h2></a>
                                                                                            <ul class="links">
                                                                                                <?php
                                                                                                foreach ($miniSubCategory as $key => $miniSub) {
                                                                                                    ?>
                                                                                                    <li><a href="<?php echo "../display/showProductByCategory.php?categoryId=" . $value[0] . "&subCategoryId=" . $sub[0] . "&miniSubCategoryId=" . $miniSub[0] . "&categoryName=" . $miniSub[1]; ?>"><?php echo $miniSub[1]; ?></a></li>

                                                                                                    <?php
                                                                                                }
                                                                                                $listMiniSubs = $miniSubCategory;
                                                                                                ?>
                                                                                            </ul>
                                                                                        </div><!-- /.col -->
                                                                                        <?php
                                                                                        $test = array();
                                                                                        $test[] = $sub;
                                                                                        $test[] = $listMiniSubs;
                                                                                        $listSubcategorys[] = $test;
                                                                                    }
                                                                                    $test1 = array();
                                                                                    $test1[] = $value;
                                                                                    $test1[] = $listSubcategorys;
                                                                                    $_SESSION['categories'][] = $test1;
                                                                                    ?>
                                                                                    </li>
                                                                                    </ul>
                                                                                    <?php
                                                                                } else {
                                                                                    $test2 = array();
                                                                                    $test2[] = $value;
                                                                                    $_SESSION['categories'][] = $test2;
                                                                                    ?>
                                                                                    <li class="active dropdown yamm-fw">
                                                                                        <a href="<?php echo "../display/showProductByCategory.php?categoryId=" . $value[0] . "&categoryName=" . $value[1]; ?>"><?php echo $value[1] ?></a>
                                                                                        <?php
                                                                                    }
                                                                                    ?>

                                                                                </li>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                            </ul>
                                                                            <div class="clearfix"></div>				
                                                                        </div><!-- /.nav-outer -->
                                                                    </div><!-- /.navbar-collapse -->
                                                                    <?php
                                                                    
                                                                    ?>

                                                                </div><!-- /.nav-bg-class -->
                                                            </div><!-- /.navbar-default -->
                                                        </div><!-- /.container-class -->
                                                        </div>

                                                        <!-- ============================================== NAVBAR : END ============================================== -->

                                                        </header>
                                                        </body>
                                                        </html>